package lucraft.mods.heroes.ironman;

import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan;
import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan.IronManCapabilityStorage;
import lucraft.mods.heroes.ironman.capabilities.IIronManCapability;
import lucraft.mods.heroes.ironman.client.gui.IMGuiHandler;
import lucraft.mods.heroes.ironman.entities.IMEntities;
import lucraft.mods.heroes.ironman.items.IMItems;
import lucraft.mods.heroes.ironman.items.ItemSuitPart;
import lucraft.mods.heroes.ironman.network.IMPacketDispatcher;
import lucraft.mods.heroes.ironman.proxies.IronManProxy;
import lucraft.mods.heroes.ironman.suits.IronManSuit;
import lucraft.mods.heroes.ironman.suitsets.IMSuitSets;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

@Mod(modid = IronMan.MODID, version = IronMan.VERSION, name = IronMan.NAME, dependencies = IronMan.DEPENDENCIES)
public class IronMan {
	
	public static final String NAME = "IronMan";
	public static final String MODID = "ironman";
	public static final String VERSION = "Beta-1.12.2-1.2.6";
	public static final String DEPENDENCIES = "required-before:lucraftcore@[1.12.2-2.3.5,)";

	@SidedProxy(clientSide = "lucraft.mods.heroes.ironman.proxies.IronManClientProxy", serverSide = "lucraft.mods.heroes.ironman.proxies.IronManProxy")
	public static IronManProxy proxy;
	
	@Instance(value = IronMan.MODID)
	public static IronMan instance;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		proxy.preInit(event);
//		LucraftCore.enableModWhitelistCheck();
		
		// Init Basic Stuff
		IMSuitSets.init();
		IMEntities.init();
		
		// Capabilities
		CapabilityManager.INSTANCE.register(IIronManCapability.class, new IronManCapabilityStorage(), CapabilityIronMan.class);
		
		// Gui Handler
		NetworkRegistry.INSTANCE.registerGuiHandler(IronMan.instance, new IMGuiHandler());
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		proxy.init(event);
		
		IMPacketDispatcher.registerPackets();
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		proxy.postInit(event);
	}
	
	public static CreativeTabs TAB_IRON_MAN = new CreativeTabs("tabIronMan") {
		@Override
		@SideOnly(Side.CLIENT)
		public ItemStack createIcon() {
			return new ItemStack(IMItems.ARC_REACTOR);
		}
	};
	
	public static CreativeTabs TAB_IRON_MAN_SUITS = new CreativeTabs("tabIronManSuits") {
		@Override
		@SideOnly(Side.CLIENT)
		public ItemStack createIcon() {
			List<IronManSuit> suits = IronManRegistries.IRON_MAN_SUIT_REGISTRY.getValues();
			if(suits.size() < 1) {
				return new ItemStack(Blocks.BARRIER);
			}
			return ItemSuitPart.setIronManSuitPart(suits.get(0), new ItemStack(IMItems.SUIT_PART_CHEST));
		}
	};
}
