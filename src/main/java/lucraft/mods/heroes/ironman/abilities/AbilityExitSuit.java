package lucraft.mods.heroes.ironman.abilities;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan;
import lucraft.mods.heroes.ironman.capabilities.IIronManCapability;
import lucraft.mods.heroes.ironman.items.ItemSuitPart;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityAction;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityExitSuit extends AbilityAction
{

	private ResourceLocation icon = new ResourceLocation(IronMan.MODID, "textures/gui/exit_arrow.png");

	public AbilityExitSuit(EntityLivingBase entity)
	{
		super(entity);
	}

	@Override public boolean action()
	{
		IIronManCapability capability = entity.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);
		capability.exitSuit();
		capability.sync();
		return true;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		IIronManCapability capability = entity.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);
		mc.getRenderItem().renderItemIntoGUI(capability.getIronManSuitSetup().getPart(ItemSuitPart.IronManSuitPart.CHEST), x - 2, y);
		GlStateManager.pushMatrix();
		GlStateManager.enableAlpha();
		mc.renderEngine.bindTexture(icon);
		gui.drawTexturedModalRect((float)x + 6, y + 4, 0, 0, 10, 10);
		GlStateManager.disableAlpha();
		GlStateManager.popMatrix();
	}

	public boolean showInAbilityBar() {
		IIronManCapability capability = entity.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);
		return capability != null && !capability.getIronManSuitSetup().isEmpty();
	}
}
