package lucraft.mods.heroes.ironman.abilities;

import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan;
import lucraft.mods.heroes.ironman.capabilities.IIronManCapability;
import lucraft.mods.heroes.ironman.items.ItemSuitPart;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityAction;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityFaceplate extends AbilityAction
{

	public AbilityFaceplate(EntityLivingBase entity)
	{
		super(entity);
	}

	@Override public boolean action()
	{
		IIronManCapability capability = entity.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);
		capability.getIronManSuitSetup().setFaceplateOpen(!capability.getIronManSuitSetup().isFaceplateOpen());
		capability.sync();
		return false;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		IIronManCapability capability = entity.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);
		mc.getRenderItem().renderItemIntoGUI(capability.getIronManSuitSetup().getPart(ItemSuitPart.IronManSuitPart.FACEPLATE), x, y);
	}

	public boolean showInAbilityBar() {
		IIronManCapability capability = entity.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);
		return capability != null && !capability.getIronManSuitSetup().getPart(ItemSuitPart.IronManSuitPart.FACEPLATE).isEmpty();
	}
}
