package lucraft.mods.heroes.ironman.abilities;

import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan;
import lucraft.mods.heroes.ironman.capabilities.IIronManCapability;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityAction;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Nictogen on 4/26/18.
 */
public class AbilityToggleFlight extends AbilityAction
{

	public AbilityToggleFlight(EntityLivingBase entity)
	{
		super(entity);
	}

	@Override public boolean action()
	{
		IIronManCapability capability = entity.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);
		IronManSuitSetup suit = capability.getIronManSuitSetup();
		suit.setFlyingEnabled(!suit.isFlyingEnabled());
		capability.sync();
		return false;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		LCRenderHelper.drawIcon(mc, gui, x, y, 1, 0);
	}

	public boolean showInAbilityBar() {
		IIronManCapability capability = entity.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);
		return capability != null && !capability.getIronManSuitSetup().isEmpty() && capability.getIronManSuitSetup().hasRepulsors();
	}
}
