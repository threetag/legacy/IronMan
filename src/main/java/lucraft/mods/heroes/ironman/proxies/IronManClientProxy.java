package lucraft.mods.heroes.ironman.proxies;

import java.util.Map;

import lucraft.mods.heroes.ironman.client.render.LayerRendererIronManSuit;
import lucraft.mods.heroes.ironman.client.render.entity.RenderEntityIronMan;
import lucraft.mods.heroes.ironman.client.render.entity.RenderRepulsorShoot;
import lucraft.mods.heroes.ironman.client.render.tileentity.TileEntityRendererSuitConstructor;
import lucraft.mods.heroes.ironman.entities.EntityIronMan;
import lucraft.mods.heroes.ironman.entities.EntityRepulsorShoot;
import lucraft.mods.heroes.ironman.tileentities.TileEntitySuitConstructor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class IronManClientProxy extends IronManProxy {

	@Override
	public void preInit(FMLPreInitializationEvent event) {
		super.preInit(event);
	}
	
	@Override
	public void init(FMLInitializationEvent event) {
		super.init(event);
		
		// Entity Rendering
		RenderManager rm = Minecraft.getMinecraft().getRenderManager();
		RenderingRegistry.registerEntityRenderingHandler(EntityIronMan.class, new RenderEntityIronMan(rm));
		RenderingRegistry.registerEntityRenderingHandler(EntityRepulsorShoot.class, new RenderRepulsorShoot(rm));
		
		// LayerRenderer
		Map<String, RenderPlayer> skinMap = Minecraft.getMinecraft().getRenderManager().getSkinMap();
		RenderPlayer render;
		render = skinMap.get("default");
		render.addLayer(new LayerRendererIronManSuit(render));

		render = skinMap.get("slim");
		render.addLayer(new LayerRendererIronManSuit(render));
		
		// TESR
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySuitConstructor.class, new TileEntityRendererSuitConstructor());
	}
	
	@Override
	public void postInit(FMLPostInitializationEvent event) {
		super.postInit(event);
	}
	
}
