package lucraft.mods.heroes.ironman.entities;

import com.google.common.base.Optional;
import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.IronManUtil;
import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan;
import lucraft.mods.heroes.ironman.client.gui.IMGuiHandler;
import lucraft.mods.heroes.ironman.network.IMPacketDispatcher;
import lucraft.mods.heroes.ironman.network.MessageSyncEntity;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.scoreboard.Team;
import net.minecraft.server.management.PreYggdrasilConverter;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;

import javax.annotation.Nullable;
import java.util.UUID;

public class EntityIronMan extends EntityLiving implements IEntityAdditionalSpawnData {

	protected static final DataParameter<Optional<UUID>> OWNER_UNIQUE_ID = EntityDataManager.<Optional<UUID>> createKey(EntityIronMan.class, DataSerializers.OPTIONAL_UNIQUE_ID);
	
	private IronManSuitSetup ironManSuit = IronManSuitSetup.EMPTY_SETUP;
	
	public EntityIronMan(World worldIn) {
		this(worldIn, null);
	}
	
	public EntityIronMan(World worldIn, IronManSuitSetup suit) {
		super(worldIn);
		this.setSize(0.6F, 1.8F);
		this.ironManSuit = suit;
		this.enablePersistence();
	}

	protected void entityInit() {
		super.entityInit();
		this.dataManager.register(OWNER_UNIQUE_ID, Optional.absent());
	}
	
	public IronManSuitSetup getIronManSuitSetup() {
		return ironManSuit;
//		return ironManSuit.isEmpty() ? new IronManSuitSetup(IronManSuit.WAR_MACHINE_MARK_2) : ironManSuit;
	}
	
	@Override
	public void onEntityUpdate() {
		super.onEntityUpdate();
		
		if(!getIronManSuitSetup().isEmpty())
			getIronManSuitSetup().onUpdate();
		
		if(getIronManSuitSetup().update) {
			getIronManSuitSetup().update = false;
			
			if(this.getEntityWorld() instanceof WorldServer) {
				for(EntityPlayer players : ((WorldServer)this.getEntityWorld()).getEntityTracker().getTrackingPlayers(this)) {
					if(players instanceof EntityPlayerMP) {
						IMPacketDispatcher.sendTo(new MessageSyncEntity(this), (EntityPlayerMP) players);
					}
				}
			}
		}
		
//		EntityLivingBase owner = getOwner();
//		
//		if(owner != null && !getEntityWorld().isRemote) {
//			this.setPositionAndUpdate(owner.posX, owner.posY, owner.posZ);
//			this.motionX = owner.motionX;
//			this.motionY = owner.motionY;
//			this.motionZ = owner.motionZ;
//			this.rotationPitch = owner.rotationPitch;
//			this.rotationYaw = owner.rotationYaw;
//			this.rotationYawHead = owner.rotationYawHead;
//			this.prevRotationYawHead = owner.prevRotationYawHead;
//			this.cameraPitch = owner.cameraPitch;
//			this.prevCameraPitch = owner.prevCameraPitch;
//			this.swingProgress = owner.swingProgress;
//			this.swingProgressInt = owner.swingProgressInt;
//			this.isSwingInProgress = owner.isSwingInProgress;
//			this.limbSwing = owner.limbSwing;
//			this.limbSwingAmount = owner.limbSwingAmount;
//			this.moveForward = owner.moveForward;
//			this.moveStrafing = owner.moveStrafing;
//			this.moveVertical = owner.moveVertical;
//			this.renderYawOffset = owner.renderYawOffset;
//		}
	}
	
	@Override
	public EnumActionResult applyPlayerInteraction(EntityPlayer player, Vec3d vec, EnumHand hand) {
		if(this.getOwnerId() != null && this.getOwnerId().equals(player.getPersistentID()) && !this.getEntityWorld().isRemote && !this.isDead) {
			if(player.isSneaking()) {
//				for(ItemStack s : getIronManSuitSetup().getItems()) {
//					this.entityDropItem(s, 0.2F);
//				}
//				this.setDead();
				player.openGui(IronMan.instance, IMGuiHandler.SUIT_INFO_GUI_ID, world, (int)player.posX, (int)player.posY, this.getEntityId());
				return EnumActionResult.SUCCESS;
			} else if(IronManUtil.canWearSuit(player) && getIronManSuitSetup().isEnabled()) {
				player.getCapability(CapabilityIronMan.IRON_MAN_CAP, null).setIronManSuitSetup(getIronManSuitSetup());
				this.setDead();
				return EnumActionResult.SUCCESS;
			}
			
			return EnumActionResult.FAIL;
		}
		return super.applyPlayerInteraction(player, vec, hand);
	}

	public void writeEntityToNBT(NBTTagCompound compound) {
		super.writeEntityToNBT(compound);

		NBTTagCompound suitTag = ironManSuit.serializeNBT();
		compound.setTag("SuitSetup", suitTag);
		
		if (this.getOwnerId() == null) {
			compound.setString("OwnerUUID", "");
		} else {
			compound.setString("OwnerUUID", this.getOwnerId().toString());
		}
	}

	public void readEntityFromNBT(NBTTagCompound compound) {
		super.readEntityFromNBT(compound);
		
		ironManSuit = new IronManSuitSetup(compound.getCompoundTag("SuitSetup"));
		
		String s;

		if (compound.hasKey("OwnerUUID", 8)) {
			s = compound.getString("OwnerUUID");
		} else {
			String s1 = compound.getString("Owner");
			s = PreYggdrasilConverter.convertMobOwnerIfNeeded(this.getServer(), s1);
		}

		if (!s.isEmpty()) {
			this.setOwnerId(UUID.fromString(s));
		}
	}

	@Nullable
	public UUID getOwnerId() {
		return (UUID) ((Optional) this.dataManager.get(OWNER_UNIQUE_ID)).orNull();
	}

	public void setOwnerId(@Nullable UUID p_184754_1_) {
		this.dataManager.set(OWNER_UNIQUE_ID, Optional.fromNullable(p_184754_1_));
	}

	@Nullable
	public EntityLivingBase getOwner() {
		try {
			UUID uuid = this.getOwnerId();
			return uuid == null ? null : this.world.getPlayerEntityByUUID(uuid);
		} catch (IllegalArgumentException var2) {
			return null;
		}
	}

	public boolean isOwner(EntityLivingBase entityIn) {
		return entityIn == this.getOwner();
	}

	public Team getTeam() {
		EntityLivingBase entitylivingbase = this.getOwner();

		if (entitylivingbase != null) {
			return entitylivingbase.getTeam();
		}

		return super.getTeam();
	}

	public boolean isOnSameTeam(Entity entityIn) {
		EntityLivingBase entitylivingbase = this.getOwner();

		if (entityIn == entitylivingbase) {
			return true;
		}

		if (entitylivingbase != null) {
			return entitylivingbase.isOnSameTeam(entityIn);
		}

		return super.isOnSameTeam(entityIn);
	}

	@Override
	public EnumHandSide getPrimaryHand() {
		if(getOwner() != null)
			return getOwner().getPrimaryHand();
		return EnumHandSide.RIGHT;
	}

	@Override
	public void writeSpawnData(ByteBuf buffer) {
		NBTTagCompound nbt = new NBTTagCompound();
		this.writeEntityToNBT(nbt);
		ByteBufUtils.writeTag(buffer, nbt);
	}

	@Override
	public void readSpawnData(ByteBuf additionalData) {
		this.readEntityFromNBT(ByteBufUtils.readTag(additionalData));
	}

}
