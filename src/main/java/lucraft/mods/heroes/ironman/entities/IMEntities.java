package lucraft.mods.heroes.ironman.entities;

import lucraft.mods.heroes.ironman.IronMan;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.EntityRegistry;

public class IMEntities {

	public static int id = 0;
	
	public static void init() {
		
		EntityRegistry.registerModEntity(new ResourceLocation(IronMan.MODID, "iron_man_suit"), EntityIronMan.class, "iron_man", id++, IronMan.instance, 64, 1, true);
		EntityRegistry.registerModEntity(new ResourceLocation(IronMan.MODID, "repulsor_shoot"), EntityRepulsorShoot.class, "repulsor_shoot", id++, IronMan.instance, 64, 1, true);
		
	}
	
}
