package lucraft.mods.heroes.ironman.suits;

import lucraft.mods.heroes.ironman.IronManUtil;
import lucraft.mods.heroes.ironman.client.models.ModelIronManSuit;
import lucraft.mods.heroes.ironman.entities.EntityRepulsorShoot;
import lucraft.mods.heroes.ironman.handler.KeySyncHandler;
import lucraft.mods.heroes.ironman.items.ItemArcReactor;
import lucraft.mods.heroes.ironman.items.ItemSuitPart;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPart;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPartInfo;
import lucraft.mods.heroes.ironman.sounds.IMSoundEvents;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IronManSuitSetup implements INBTSerializable<NBTTagCompound>
{

	public static final IronManSuitSetup EMPTY_SETUP = new IronManSuitSetup(ItemStack.EMPTY, ItemStack.EMPTY, ItemStack.EMPTY, ItemStack.EMPTY, ItemStack.EMPTY,
			ItemStack.EMPTY, ItemStack.EMPTY, ItemStack.EMPTY);

	public static final int ENERGY_FOR_ACTIVATED = 15000;
	public static final int ENERGY_FOR_REPULSOR = 100000;
	public static final int ENERGY_FOR_IDLE_FLIGHT = 200;
	public static final int ENERGY_FOR_ACTIVE_FLIGHT = 500;

	protected Map<IronManSuitPart, ItemStack> parts = new HashMap<>();
	protected ItemStack arcReactor = ItemStack.EMPTY;
	protected boolean enabled;
	protected int enabledTimer;
	public static final int maxEnabledTimer = 20;
	public boolean update;
	protected int energyTimer = 0;
	protected static final int maxEnergyTimer = 20 * 60;
	protected boolean faceplateOpen;
	protected int faceplateTimer;
	public static final int maxFaceplateTimer = 20;
	protected boolean flying;
	protected float damageReduction = 0F;
	protected float verticalFlyingSpeed = 0F;
	protected float horizontalFlyingSpeed = 0F;

	public IronManSuitSetup(ItemStack helmet, ItemStack faceplate, ItemStack chest, ItemStack rightArm, ItemStack leftArm, ItemStack rightLeg,
			ItemStack leftLeg, ItemStack reactor)
	{
		parts.put(IronManSuitPart.HELMET, helmet);
		parts.put(IronManSuitPart.FACEPLATE, faceplate);
		parts.put(IronManSuitPart.CHEST, chest);
		parts.put(IronManSuitPart.RIGHT_ARM, rightArm);
		parts.put(IronManSuitPart.LEFT_ARM, leftArm);
		parts.put(IronManSuitPart.RIGHT_LEG, rightLeg);
		parts.put(IronManSuitPart.LEFT_LEG, leftLeg);
		this.arcReactor = reactor;

		this.updateStats();
	}

	public IronManSuitSetup(NBTTagCompound nbt)
	{
		this.deserializeNBT(nbt);
	}

	public void updateStats()
	{
		float dR = 0F;
		float vFS = 0;
		int vFS_ = 0;
		float hFS = 0;
		int hFS_ = 0;

		for (IronManSuitPart part : parts.keySet())
		{
			ItemStack stack = parts.get(part);

			if (IronManUtil.isValidSuitPart(stack, part))
			{
				IronManSuit suit = ItemSuitPart.getIronManSuit(stack);
				IronManSuitPartInfo info = suit.getSuitPartInfo(part);
				dR += info.getDamageReduction();
				vFS += info.getVerticalFlyingSpeed();
				if (info.getVerticalFlyingSpeed() > 0F)
					vFS_++;
				hFS += info.getHorizontalFlyingSpeed();
				if (info.getHorizontalFlyingSpeed() > 0F)
					hFS_++;
			}
		}

		this.damageReduction = dR;
		this.verticalFlyingSpeed = vFS_ == 0 ? vFS : (vFS / (float) vFS_);
		this.horizontalFlyingSpeed = hFS_ == 0 ? hFS : (hFS / (float) hFS_);
	}

	public void onUpdate()
	{
		if (faceplateOpen && faceplateTimer < maxFaceplateTimer)
			faceplateTimer++;
		else if (!faceplateOpen && faceplateTimer > 0)
			faceplateTimer--;

		if (enabled && enabledTimer < maxEnabledTimer)
			enabledTimer++;
		else if (!enabled && enabledTimer > 0)
			enabledTimer--;

		if ((getReactor().isEmpty() || getEnergy() <= 0) && isEnabled())
		{
			setEnabled(false);
			markDirty();
		}

		if (!isEnabled())
		{
			if (isFlyingEnabled())
			{
				this.setFlyingEnabled(false);
				this.markDirty();
			}
		}

		if (!getReactor().isEmpty() && getReactor().getItem() instanceof ItemArcReactor && ((ItemArcReactor) getReactor().getItem()).updateEnergy(getReactor()))
			markDirty();

		if (energyTimer >= maxEnergyTimer)
		{
			energyTimer = 0;
			if (isEnabled())
				drainEnergy(ENERGY_FOR_ACTIVATED, false);
		}
		else
		{
			energyTimer++;
		}
	}

	public void markDirty()
	{
		this.update = true;
	}

	public void handleFlight(EntityLivingBase player)
	{
		boolean flyKeyDown = KeySyncHandler.isFlyKeyDown(player);

		int lowestY = player.getPosition().getY();

		while (lowestY > 0 && !player.world.isBlockFullCube(new BlockPos(player.posX, lowestY, player.posZ)))
		{
			lowestY--;
		}

		double motionX = 0;
		double motionY = 0;
		double motionZ = 0;

		if (!getPart(IronManSuitPart.RIGHT_LEG).isEmpty() && getPart(IronManSuitPart.LEFT_LEG).isEmpty())
		{
			Vec3d v = player.getLookVec().rotateYaw((float) Math.toRadians(90));
			motionX = v.x / 20D;
			motionZ = v.z / 20D;
		}
		else if (getPart(IronManSuitPart.RIGHT_LEG).isEmpty() && !getPart(IronManSuitPart.LEFT_LEG).isEmpty())
		{
			Vec3d v = player.getLookVec().rotateYaw((float) Math.toRadians(-90));
			motionX = v.x / 20D;
			motionZ = v.z / 20D;
		}

		if (!getPart(IronManSuitPart.RIGHT_ARM).isEmpty() && getPart(IronManSuitPart.LEFT_ARM).isEmpty())
		{
			Vec3d v = player.getLookVec().rotateYaw((float) Math.toRadians(90));
			motionX = v.x / 20D;
			motionZ = v.z / 20D;
		}
		else if (getPart(IronManSuitPart.RIGHT_ARM).isEmpty() && !getPart(IronManSuitPart.LEFT_ARM).isEmpty())
		{
			Vec3d v = player.getLookVec().rotateYaw((float) Math.toRadians(-90));
			motionX = v.x / 20D;
			motionZ = v.z / 20D;
		}

		if (flyKeyDown && !player.onGround)
		{
			Vec3d vec = player.getLookVec();
			player.motionX = (vec.x * getHorizontalFlyingSpeed()) + motionX * 5D;
			motionY += vec.y * getVerticalFlyingSpeed();
			player.motionZ = (vec.z * getHorizontalFlyingSpeed()) + motionZ * 5D;

			if (!player.world.isRemote)
			{
				player.fallDistance = 0.0F;
			}

			this.drainEnergy(ENERGY_FOR_ACTIVE_FLIGHT, false);
		}
		else
		{
			if (player.getPosition().getY() - lowestY < 5)
			{
				motionY += 1;
			}
			//TO DO realistic shots, no nobraincontroll, better system for my dick and 3458345� for Tim
			motionY += Math.sin(player.ticksExisted / 10F) / 100F;
			player.fallDistance = 0F;

			player.motionX += motionX;
			player.motionZ += motionZ;

			this.drainEnergy(ENERGY_FOR_IDLE_FLIGHT, false);
		}

		player.motionY = motionY;

		//		if(energyTimer % 2 == 0 && !player.world.isRemote) {
		//			float speed = (float) MathHelper.clamp(Math.sqrt((player.prevPosX - player.posX) * (player.prevPosX - player.posX) + (player.prevPosZ - player.posZ) * (player.prevPosZ - player.posZ)), 0F, 1F) * (90F + player.rotationPitch);
		//			Vec3d vec = IronManUtil.rotateZ(new Vec3d(-0.13D, player.height * 0.4D, 0.4D), (float) Math.toRadians(speed)).rotateYaw(-player.renderYawOffset * 0.017453292F - ((float) Math.PI / 2F));
		//			Vec3d vec1 = IronManUtil.rotateZ(new Vec3d(-0.13D, player.height * 0.4D, -0.4D), (float) Math.toRadians(speed)).rotateYaw(-player.renderYawOffset * 0.017453292F - ((float) Math.PI / 2F));
		//
		//			LucraftCoreUtil.spawnParticle(EnumParticleTypes.FLAME, player.posX + vec.x, player.posY + vec.y, player.posZ + vec.z, 0, -0.05F, 0, player.dimension);
		//			LucraftCoreUtil.spawnParticle(EnumParticleTypes.FLAME, player.posX + vec1.x, player.posY + vec1.y, player.posZ + vec1.z, 0, -0.05F, 0, player.dimension);
		//			LucraftCoreUtil.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, player.posX + vec.x, player.posY + vec.y, player.posZ + vec.z, 0, -0.05F, 0, player.dimension);
		//			LucraftCoreUtil.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, player.posX + vec1.x, player.posY + vec1.y, player.posZ + vec1.z, 0, -0.05F, 0, player.dimension);
		//		}
	}

	public void repulsorShoot(EntityLivingBase entity)
	{
		if (!entity.world.isRemote && !getPart(entity.getPrimaryHand() == EnumHandSide.RIGHT ? IronManSuitPart.RIGHT_ARM : IronManSuitPart.LEFT_ARM).isEmpty()
				&& drainEnergy(ENERGY_FOR_REPULSOR, true) >= ENERGY_FOR_REPULSOR)
		{
			EntityRepulsorShoot shoot = new EntityRepulsorShoot(entity.world, entity);
			shoot.shoot(entity, entity.rotationPitch, entity.rotationYaw, 0.0F, 1.5F, 1.0F);
			entity.world.spawnEntity(shoot);
			shoot.damage = 6;
			drainEnergy(ENERGY_FOR_REPULSOR, false);
			PlayerHelper.playSoundToAll(entity.world, entity.posX, entity.posY, entity.posZ, 50D, IMSoundEvents.REPULSOR_SHOT, SoundCategory.PLAYERS);
		}
	}

	public void setParts(IronManSuitPart part, ItemStack stack)
	{
		if (IronManUtil.isValidSuitPart(stack, part))
		{
			parts.put(part, stack);
			this.updateStats();
		}
	}

	public void setPartEmpty(IronManSuitPart part)
	{
		parts.put(part, ItemStack.EMPTY);
		this.updateStats();
	}

	public ItemStack getPart(IronManSuitPart part)
	{
		return parts.containsKey(part) ? parts.get(part) : ItemStack.EMPTY;
	}

	public Boolean hasRepulsors()
	{
		ArrayList<ItemStack> parts = new ArrayList<>();
		parts.add(getPart(IronManSuitPart.LEFT_ARM));
		parts.add(getPart(IronManSuitPart.RIGHT_ARM));
		parts.add(getPart(IronManSuitPart.LEFT_LEG));
		parts.add(getPart(IronManSuitPart.RIGHT_LEG));

		for (ItemStack part : parts)
		{
			if (!part.isEmpty())
				return true;
		}
		return false;
	}

	public void setArcReactor(ItemStack stack)
	{
		if (IronManUtil.isValidReactor(stack))
			this.arcReactor = stack;
	}

	public ItemStack getReactor()
	{
		return this.arcReactor == null ? ItemStack.EMPTY : this.arcReactor;
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
		this.markDirty();
	}

	public boolean isEnabled()
	{
		return this.enabled;
	}

	public int getEnabledTimer()
	{
		return this.enabledTimer;
	}

	public void setFaceplateOpen(boolean faceplateOpen)
	{
		this.faceplateOpen = faceplateOpen;
	}

	public boolean isFaceplateOpen()
	{
		return faceplateOpen;
	}

	public int getFaceplateTimer()
	{
		return faceplateTimer;
	}

	public float getDamageReduction()
	{
		return damageReduction;
	}

	public float getVerticalFlyingSpeed()
	{
		return verticalFlyingSpeed;
	}

	public float getHorizontalFlyingSpeed()
	{
		return horizontalFlyingSpeed;
	}

	public boolean isFlyingEnabled()
	{
		if(!hasRepulsors()) this.flying = false;
		return flying;
	}

	public void setFlyingEnabled(boolean flying)
	{
		this.flying = flying;
	}

	public boolean isEmpty()
	{
		boolean b = false;
		for (IronManSuitPart part : parts.keySet())
		{
			if (parts.get(part) != null && !parts.get(part).isEmpty())
			{
				b = true;
			}
		}
		if (!getReactor().isEmpty())
			b = true;
		return this == IronManSuitSetup.EMPTY_SETUP || !b;
	}

	public List<ItemStack> getItems()
	{
		List<ItemStack> items = new ArrayList<>();
		for (IronManSuitPart p : this.parts.keySet())
		{
			if (!parts.get(p).isEmpty())
			{
				items.add(parts.get(p));
			}
		}
		if (!getReactor().isEmpty())
			items.add(getReactor());
		return items;
	}

	public int getEnergy()
	{
		if (!getReactor().isEmpty() && getReactor().hasCapability(CapabilityEnergy.ENERGY, null))
			return getReactor().getCapability(CapabilityEnergy.ENERGY, null).getEnergyStored();
		return 0;
	}

	public int getMaxEnergy()
	{
		if (!getReactor().isEmpty() && getReactor().hasCapability(CapabilityEnergy.ENERGY, null))
			return getReactor().getCapability(CapabilityEnergy.ENERGY, null).getMaxEnergyStored();
		return 0;
	}

	public int receiveEnergy(int amount, boolean simulate)
	{
		if (!getReactor().isEmpty() && getReactor().hasCapability(CapabilityEnergy.ENERGY, null))
		{
			int i = getReactor().getCapability(CapabilityEnergy.ENERGY, null).receiveEnergy(amount, simulate);
			if (i > 0)
				markDirty();
			return i;
		}

		return 0;
	}

	public int drainEnergy(int amount, boolean simulate)
	{
		if (!getReactor().isEmpty() && getReactor().hasCapability(CapabilityEnergy.ENERGY, null))
		{
			int i = getReactor().getCapability(CapabilityEnergy.ENERGY, null).extractEnergy(amount, simulate);
			if (i > 0)
				markDirty();
			return i;
		}

		return 0;
	}

	@SideOnly(Side.CLIENT)
	public void renderSuit(EntityLivingBase entity, RenderLivingBase<?> renderer, ModelBiped originalModel, float limbSwing, float limbSwingAmount,
			float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		this.renderSuit(entity, renderer, originalModel, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, scale, 1, 1);
	}

	@SideOnly(Side.CLIENT)
	public void renderSuit(EntityLivingBase entity, RenderLivingBase<?> renderer, ModelBiped originalModel, float limbSwing, float limbSwingAmount,
			float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale, float suitAlpha, float glowAlpha)
	{
		ModelIronManSuit model = new ModelIronManSuit(0.01F, entity instanceof EntityPlayer ? PlayerHelper.hasSmallArms((EntityPlayer) entity) : false);

		if (originalModel != null)
			model.setModelAttributes(originalModel);

		model.isChild = false;
		model.setLivingAnimations(entity, limbSwing, limbSwingAmount, partialTicks);
		float faceplateRotation = (float) Math.toRadians(((float) getFaceplateTimer() / (float) maxFaceplateTimer) * 80F);
		// model.setFaceplateRotation((float)
		// -Math.toRadians((MathHelper.sin((player.ticksExisted + partialTicks)
		// / 10F) + 1F) / 2F * 80F));

		model.setFaceplateRotation(faceplateRotation);

		GlStateManager.pushMatrix();
		GL11.glDisable(GL11.GL_CULL_FACE);
		GlStateManager.enableBlend();
		GL11.glBlendFunc(770, 771);
		GL11.glAlphaFunc(516, 0.003921569F);

		for (IronManSuitPart p : this.parts.keySet())
		{
			if (IronManUtil.isValidSuitPart(this.parts.get(p), p))
			{
				ItemStack stack = this.parts.get(p);
				IronManSuit suit = ItemSuitPart.getIronManSuit(stack);
				if (suit == null)
					break;
				p.setModelVisibilities(model);
				GlStateManager.color(1, 1, 1, suitAlpha);

				if (renderer != null)
					renderer.bindTexture(suit.getSuitTexture(false));
				else
					Minecraft.getMinecraft().renderEngine.bindTexture(suit.getSuitTexture(false));
				model.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);

				GlStateManager.disableLighting();
				LCRenderHelper.setLightmapTextureCoords(240, 240);
				GlStateManager.color(1, 1, 1, glowAlpha);

				if (renderer != null)
					renderer.bindTexture(suit.getSuitTexture(true));
				else
					Minecraft.getMinecraft().renderEngine.bindTexture(suit.getSuitTexture(true));
				model.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);

				LCRenderHelper.restoreLightmapTextureCoords();

				GlStateManager.enableLighting();
			}
		}

		GlStateManager.disableBlend();
		GL11.glEnable(GL11.GL_CULL_FACE);
		GlStateManager.popMatrix();
	}

	@Override
	public NBTTagCompound serializeNBT()
	{
		NBTTagCompound nbt = new NBTTagCompound();

		NBTTagCompound partsNbt = new NBTTagCompound();
		for (IronManSuitPart parts : this.parts.keySet())
		{
			NBTTagCompound item = this.parts.get(parts).serializeNBT();
			partsNbt.setTag(parts.toString().toLowerCase(), item);
		}
		nbt.setTag("Parts", partsNbt);

		if (!getReactor().isEmpty())
		{
			NBTTagCompound reactorTag = getReactor().serializeNBT();
			nbt.setTag("Reactor", reactorTag);
		}

		nbt.setBoolean("Enabled", enabled);
		nbt.setInteger("EnabledTimer", enabledTimer);
		nbt.setInteger("EnergyTimer", energyTimer);
		nbt.setBoolean("FaceplateOpen", faceplateOpen);
		nbt.setInteger("FaceplateTimer", faceplateTimer);
		nbt.setBoolean("Flying", flying);

		return nbt;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		NBTTagCompound partsNbt = nbt.getCompoundTag("Parts");
		for (IronManSuitPart parts : IronManSuitPart.values())
		{
			NBTTagCompound item = partsNbt.getCompoundTag(parts.toString().toLowerCase());
			this.setParts(parts, new ItemStack(item));
		}

		this.setArcReactor(new ItemStack(nbt.getCompoundTag("Reactor")));

		enabled = nbt.getBoolean("Enabled");
		enabledTimer = nbt.getInteger("EnabledTimer");
		energyTimer = nbt.getInteger("EnergyTimer");
		faceplateOpen = nbt.getBoolean("FaceplateOpen");
		faceplateTimer = nbt.getInteger("FaceplateTimer");
		flying = nbt.getBoolean("Flying");

		this.updateStats();
	}

}
