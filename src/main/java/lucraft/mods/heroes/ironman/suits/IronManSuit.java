package lucraft.mods.heroes.ironman.suits;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPart;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPartInfo;
import lucraft.mods.heroes.ironman.network.IMPacketDispatcher;
import lucraft.mods.heroes.ironman.network.MessageSyncIronManSuit;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.addonpacks.AddonPackReadEvent;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EventBusSubscriber(modid = IronMan.MODID)
public class IronManSuit extends net.minecraftforge.registries.IForgeRegistryEntry.Impl<IronManSuit> {

	public static List<IronManSuit> SUITS = new ArrayList<>();

	@SubscribeEvent
	public static void onAddonPackRead(AddonPackReadEvent e) {
		if (e.getDirectory().equals("ironman_suits") && FilenameUtils.getExtension(e.getFileName()).equalsIgnoreCase("json")) {
			try {
				BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(e.getInputStream(), StandardCharsets.UTF_8));
				JsonObject jsonobject = (new JsonParser()).parse(bufferedreader).getAsJsonObject();
				IronManSuit suit = new IronManSuit();
				suit.jsonOriginal = jsonobject;
				suit.deserialize(jsonobject);
				suit.setRegistryName(e.getResourceLocation());
				SUITS.add(suit);
			} catch (Exception e2) {
				LucraftCore.LOGGER.error("Wasn't able to read IronMan suit '" + e.getFileName() + "' in addon pack '" + e.getPackFile().getName() + "': " + e2.getMessage());
			}
		}
	}

	@SubscribeEvent
	public static void onRegisterIronManSuits(RegistryEvent.Register<IronManSuit> e) {
		for (IronManSuit suits : SUITS)
			e.getRegistry().register(suits);
	}

	@SubscribeEvent
	public static void onLogin(PlayerEvent.PlayerLoggedInEvent e) {
		if (e.player instanceof EntityPlayerMP) {
			for (IronManSuit suits : SUITS) {
				IMPacketDispatcher.sendTo(new MessageSyncIronManSuit(suits, suits.jsonOriginal), (EntityPlayerMP) e.player);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------------------

	protected ResourceLocation texturePath;
	protected ResourceLocation texturePathGlow;
	protected ITextComponent unlocalizedName = new TextComponentString("");
	protected Map<IronManSuitPart, IronManSuitPartInfo> suitPartInfo = new HashMap<IronManSuitPart, IronManSuitPartInfo>();
	public JsonObject jsonOriginal;

	public IronManSuit() {

	}

	public IronManSuit setSuitTexturePath(ResourceLocation texturePath, boolean glow) {
		if (glow)
			this.texturePathGlow = texturePath;
		else
			this.texturePath = texturePath;
		return this;
	}

	public IronManSuit setUnlocalizedName(ITextComponent name) {
		this.unlocalizedName = name;
		return this;
	}

	public ITextComponent getDisplayName() {
		return unlocalizedName;
	}

	public ResourceLocation getSuitTexture(boolean glow) {
		return glow ? texturePathGlow : texturePath;
	}

	public IronManSuit setSuitPartInfo(IronManSuitPart part, IronManSuitPartInfo info) {
		this.suitPartInfo.put(part, info);
		return this;
	}

	public IronManSuitPartInfo getSuitPartInfo(IronManSuitPart part) {
		return suitPartInfo.containsKey(part) ? suitPartInfo.get(part) : null;
	}

	public IronManSuit deserialize(JsonObject json) {
		ITextComponent name = ITextComponent.Serializer.jsonToComponent(JsonUtils.getJsonObject(json, "name").toString());
		this.setUnlocalizedName(name);

		JsonObject parts = JsonUtils.getJsonObject(json, "parts");
		for(IronManSuitPart part : IronManSuitPart.values())
			this.setSuitPartInfo(part, IronManSuitPartInfo.deserialize(JsonUtils.getJsonObject(parts, part.toString().toLowerCase())));

		if (JsonUtils.hasField(json, "textures")) {
			JsonObject texture = JsonUtils.getJsonObject(json, "textures");
			this.setSuitTexturePath(new ResourceLocation(JsonUtils.getString(texture, "normal")), false);
			this.setSuitTexturePath(new ResourceLocation(JsonUtils.getString(texture, "glow")), true);
		}

		return this;
	}

}
