package lucraft.mods.heroes.ironman.container.slots;

import lucraft.mods.heroes.ironman.container.ContainerArcReactor;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotPalladiumCore extends Slot {

	public ContainerArcReactor parent;

	public SlotPalladiumCore(ContainerArcReactor container, int index, int xPosition, int yPosition) {
		super(container.getInventoryArcReactor(), index, xPosition, yPosition);
		this.parent = container;
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		return parent.getInventoryArcReactor().isItemValidForSlot(slotNumber, stack);
	}

	@Override
	public ItemStack getStack() {
		return this.parent.getInventoryArcReactor().getStackInSlot(this.getSlotIndex());
	}

	@Override
	public void putStack(ItemStack stack) {
		this.parent.getInventoryArcReactor().setInventorySlotContents(this.getSlotIndex(), stack);
		this.onSlotChanged();
	}

	@Override
	public void onSlotChanged() {
		this.parent.getInventoryArcReactor().markDirty();
	}

	@Override
	public int getSlotStackLimit() {
		return this.parent.getInventoryArcReactor().getInventoryStackLimit();
	}

	@Override
	public ItemStack decrStackSize(int amount) {
		return this.parent.getInventoryArcReactor().decrStackSize(this.getSlotIndex(), amount);
	}

	@Override
	public boolean isHere(IInventory inv, int slotIn) {
		return inv == this.parent.getInventoryArcReactor() && slotIn == this.getSlotIndex();
	}

	@Override
	public boolean isSameInventory(Slot other) {
		return this.parent.getInventoryArcReactor() == other.inventory;
	}

}
