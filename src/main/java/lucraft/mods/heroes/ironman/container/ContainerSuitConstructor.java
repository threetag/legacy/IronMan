package lucraft.mods.heroes.ironman.container;

import lucraft.mods.heroes.ironman.IronManUtil;
import lucraft.mods.heroes.ironman.items.ItemSuitPart;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPart;
import lucraft.mods.heroes.ironman.tileentities.TileEntitySuitConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.energy.CapabilityEnergy;

public class ContainerSuitConstructor extends Container {

	private final TileEntitySuitConstructor tileEntity;

	public ContainerSuitConstructor(EntityPlayer player, TileEntitySuitConstructor tileEntity) {
		this.tileEntity = tileEntity;

		this.addSlotToContainer(new SlotSuitConstructor(tileEntity, 0, 33, 14, IronManSuitPart.HELMET));
		this.addSlotToContainer(new SlotSuitConstructor(tileEntity, 1, 55, 14, IronManSuitPart.FACEPLATE));
		this.addSlotToContainer(new SlotSuitConstructor(tileEntity, 2, 44, 38, IronManSuitPart.CHEST));
		this.addSlotToContainer(new SlotSuitConstructor(tileEntity, 3, 19, 42, IronManSuitPart.RIGHT_ARM));
		this.addSlotToContainer(new SlotSuitConstructor(tileEntity, 4, 69, 42, IronManSuitPart.LEFT_ARM));
		this.addSlotToContainer(new SlotSuitConstructor(tileEntity, 5, 19, 74, IronManSuitPart.RIGHT_LEG));
		this.addSlotToContainer(new SlotSuitConstructor(tileEntity, 6, 69, 74, IronManSuitPart.LEFT_LEG));
		
		this.addSlotToContainer(new Slot(tileEntity, 7, 94, 42) {
			@Override
			public boolean isItemValid(ItemStack stack) {
				return IronManUtil.isValidReactor(stack);
			}
		});

		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 9; ++j) {
				this.addSlotToContainer(new Slot(player.inventory, j + i * 9 + 9, 8 + j * 18, 99 + i * 18));
			}
		}

		for (int k = 0; k < 9; ++k) {
			this.addSlotToContainer(new Slot(player.inventory, k, 8 + k * 18, 157));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index <= 7) {
				if (!this.mergeItemStack(itemstack1, 8, 44, true)) {
					return ItemStack.EMPTY;
				}

				slot.onSlotChange(itemstack1, itemstack);
			} else {
				if (itemstack1.getItem() instanceof ItemSuitPart && ((ItemSuitPart)itemstack1.getItem()).part == IronManSuitPart.HELMET) {
					if (!this.mergeItemStack(itemstack1, 0, 1, false)) {
						return ItemStack.EMPTY;
					}
				} else if (itemstack1.getItem() instanceof ItemSuitPart && ((ItemSuitPart)itemstack1.getItem()).part == IronManSuitPart.FACEPLATE) {
					if (!this.mergeItemStack(itemstack1, 1, 2, false)) {
						return ItemStack.EMPTY;
					}
				} else if (itemstack1.getItem() instanceof ItemSuitPart && ((ItemSuitPart)itemstack1.getItem()).part == IronManSuitPart.CHEST) {
					if (!this.mergeItemStack(itemstack1, 2, 3, false)) {
						return ItemStack.EMPTY;
					}
				} else if (itemstack1.getItem() instanceof ItemSuitPart && ((ItemSuitPart)itemstack1.getItem()).part == IronManSuitPart.RIGHT_ARM) {
					if (!this.mergeItemStack(itemstack1, 3, 4, false)) {
						return ItemStack.EMPTY;
					}
				} else if (itemstack1.getItem() instanceof ItemSuitPart && ((ItemSuitPart)itemstack1.getItem()).part == IronManSuitPart.LEFT_ARM) {
					if (!this.mergeItemStack(itemstack1, 4, 5, false)) {
						return ItemStack.EMPTY;
					}
				} else if (itemstack1.getItem() instanceof ItemSuitPart && ((ItemSuitPart)itemstack1.getItem()).part == IronManSuitPart.RIGHT_LEG) {
					if (!this.mergeItemStack(itemstack1, 5, 6, false)) {
						return ItemStack.EMPTY;
					}
				} else if (itemstack1.getItem() instanceof ItemSuitPart && ((ItemSuitPart)itemstack1.getItem()).part == IronManSuitPart.LEFT_LEG) {
					if (!this.mergeItemStack(itemstack1, 6, 7, false)) {
						return ItemStack.EMPTY;
					}
				} else if (itemstack1.hasCapability(CapabilityEnergy.ENERGY, null)) {
					if (!this.mergeItemStack(itemstack1, 7, 8, false)) {
						return ItemStack.EMPTY;
					}
				} else if (index >= 8 && index < 35) {
					if (!this.mergeItemStack(itemstack1, 35, 44, false)) {
						return ItemStack.EMPTY;
					}
				} else if (index >= 35 && index < 44 && !this.mergeItemStack(itemstack1, 8, 35, false)) {
					return ItemStack.EMPTY;
				}
			}

			if (itemstack1.isEmpty()) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}

			slot.onTake(playerIn, itemstack1);
		}

		return itemstack;
	}

	public static class SlotSuitConstructor extends Slot {

		public final IronManSuitPart part;

		public SlotSuitConstructor(IInventory inventoryIn, int index, int xPosition, int yPosition, IronManSuitPart part) {
			super(inventoryIn, index, xPosition, yPosition);
			this.part = part;
		}

		@Override
		public boolean isItemValid(ItemStack stack) {
			return stack.getItem() instanceof ItemSuitPart && ((ItemSuitPart) stack.getItem()).part == part;
		}

	}

}
