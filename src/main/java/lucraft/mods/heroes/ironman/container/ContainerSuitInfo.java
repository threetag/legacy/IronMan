package lucraft.mods.heroes.ironman.container;

import lucraft.mods.heroes.ironman.entities.EntityIronMan;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPart;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerSuitInfo extends Container {

	public ContainerSuitInfo(EntityPlayer player, EntityIronMan entity) {
		this.addSlotToContainer(new SlotCantTake(entity.getIronManSuitSetup(), IronManSuitPart.HELMET, 33, 14));
		this.addSlotToContainer(new SlotCantTake(entity.getIronManSuitSetup(), IronManSuitPart.FACEPLATE, 55, 14));
		this.addSlotToContainer(new SlotCantTake(entity.getIronManSuitSetup(), IronManSuitPart.CHEST, 44, 38));
		this.addSlotToContainer(new SlotCantTake(entity.getIronManSuitSetup(), IronManSuitPart.RIGHT_ARM, 19, 42));
		this.addSlotToContainer(new SlotCantTake(entity.getIronManSuitSetup(), IronManSuitPart.LEFT_ARM, 69, 42));
		this.addSlotToContainer(new SlotCantTake(entity.getIronManSuitSetup(), IronManSuitPart.RIGHT_LEG, 19, 74));
		this.addSlotToContainer(new SlotCantTake(entity.getIronManSuitSetup(), IronManSuitPart.LEFT_LEG, 69, 74));
		
		this.addSlotToContainer(new SlotArcReactor(entity.getIronManSuitSetup(), 94, 42));
		
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 9; ++j) {
				this.addSlotToContainer(new Slot(player.inventory, j + i * 9 + 9,48 + j * 18, 116 + i * 18));
			}
		}

		for (int k = 0; k < 9; ++k) {
			this.addSlotToContainer(new Slot(player.inventory, k, 48 + k * 18, 174));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		return ItemStack.EMPTY;
	}

	public static class SlotCantTake extends Slot {

		public IronManSuitSetup setup;
		public IronManSuitPart part;
		
		public SlotCantTake(IronManSuitSetup setup, IronManSuitPart part, int xPosition, int yPosition) {
			super(null, 0, xPosition, yPosition);
			this.setup = setup;
			this.part = part;
		}

		@Override
		public boolean canTakeStack(EntityPlayer playerIn) {
			return false;
		}
		
		@Override
		public ItemStack getStack() {
			return setup.getPart(part);
		}

		@Override
		public void putStack(ItemStack stack) {
			setup.setParts(part, stack);
			this.onSlotChanged();
		}

		@Override
		public void onSlotChanged() {
			setup.markDirty();
		}

		@Override
		public int getSlotStackLimit() {
			return 1;
		}

		@Override
		public ItemStack decrStackSize(int amount) {
			if (setup.getPart(part).isEmpty())
				return ItemStack.EMPTY;
			setup.getPart(part).shrink(amount);
			return setup.getPart(part);
		}

		@Override
		public boolean isHere(IInventory inv, int slotIn) {
			return false;
		}

		@Override
		public boolean isSameInventory(Slot other) {
			return false;
		}

	}

	public static class SlotArcReactor extends Slot {

		public IronManSuitSetup setup;

		public SlotArcReactor(IronManSuitSetup setup, int xPosition, int yPosition) {
			super(null, 0, xPosition, yPosition);
			this.setup = setup;
		}

		@Override
		public ItemStack getStack() {
			return setup.getReactor();
		}

		@Override
		public void putStack(ItemStack stack) {
			setup.setArcReactor(stack);
			this.onSlotChanged();
		}

		@Override
		public void onSlotChanged() {
			setup.markDirty();
		}

		@Override
		public int getSlotStackLimit() {
			return 1;
		}

		@Override
		public ItemStack decrStackSize(int amount) {
			if (setup.getReactor().isEmpty())
				return ItemStack.EMPTY;
			return setup.getReactor().splitStack(amount);
		}

		@Override
		public boolean isHere(IInventory inv, int slotIn) {
			return false;
		}

		@Override
		public boolean isSameInventory(Slot other) {
			return false;
		}

	}

}
