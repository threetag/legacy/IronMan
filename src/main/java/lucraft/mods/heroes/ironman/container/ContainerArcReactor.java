package lucraft.mods.heroes.ironman.container;

import lucraft.mods.heroes.ironman.container.slots.SlotPalladiumCore;
import lucraft.mods.heroes.ironman.items.InventoryArcReactor;
import lucraft.mods.heroes.ironman.items.ItemPalladiumCore;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerArcReactor extends Container {

	private InventoryArcReactor inventory;

	public ContainerArcReactor(EntityPlayer player, InventoryArcReactor inventory) {
		this.inventory = inventory;

		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 9; ++j) {
				this.addSlotToContainer(new Slot(player.inventory, j + i * 9 + 9, 8 + j * 18, 141 + i * 18));
			}
		}

		for (int k = 0; k < 9; ++k) {
			this.addSlotToContainer(new Slot(player.inventory, k, 8 + k * 18, 199) {
				@Override
				public boolean canTakeStack(EntityPlayer playerIn) {
					return this.getStack() != player.getHeldItemMainhand();
				}
			});
		}

		this.addSlotToContainer(new SlotPalladiumCore(this, 0, 80, 57));
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}

	public InventoryArcReactor getInventoryArcReactor() {
		return inventory;
	}

	@Override
	public void detectAndSendChanges() {
		for (int i = 0; i < this.inventorySlots.size(); ++i) {
			ItemStack itemstack = ((Slot) this.inventorySlots.get(i)).getStack();
			ItemStack itemstack1 = this.inventoryItemStacks.get(i);

			if (!ItemStack.areItemStacksEqual(itemstack1, itemstack)) {
				itemstack1 = itemstack.isEmpty() ? ItemStack.EMPTY : itemstack.copy();
				this.inventoryItemStacks.set(i, itemstack1);
				this.inventory = new InventoryArcReactor(this.inventory.stack);

				for (int j = 0; j < this.listeners.size(); ++j) {
					((IContainerListener) this.listeners.get(j)).sendSlotContents(this, i, itemstack1);
				}
			}
		}
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index == 36) {
				if (!this.mergeItemStack(itemstack1, 0, 36, true)) {
					return ItemStack.EMPTY;
				}

				slot.onSlotChange(itemstack1, itemstack);
			} else {
				if (itemstack1.getItem() instanceof ItemPalladiumCore) {
					if (!this.mergeItemStack(itemstack1, 36, 37, false)) {
						return ItemStack.EMPTY;
					}
				} else if (index >= 0 && index < 27) {
					if (!this.mergeItemStack(itemstack1, 27, 36, false)) {
						return ItemStack.EMPTY;
					}
				} else if (index >= 27 && index < 36 && !this.mergeItemStack(itemstack1, 0, 27, false)) {
					return ItemStack.EMPTY;
				}
			}

			if (itemstack1.isEmpty()) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}

			slot.onTake(playerIn, itemstack1);
		}

		return itemstack;
	}

}
