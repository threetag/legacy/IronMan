package lucraft.mods.heroes.ironman.capabilities;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.IronManUtil;
import lucraft.mods.heroes.ironman.entities.EntityIronMan;
import lucraft.mods.heroes.ironman.handler.KeySyncHandler;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPart;
import lucraft.mods.heroes.ironman.network.IMPacketDispatcher;
import lucraft.mods.heroes.ironman.network.MessageSyncIronManCapability;
import lucraft.mods.heroes.ironman.sounds.IMSoundEvents;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import lucraft.mods.lucraftcore.util.events.PlayerEmptyClickEvent;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;

import java.util.UUID;

@EventBusSubscriber(modid = IronMan.MODID)
public class CapabilityIronMan implements IIronManCapability
{

	@CapabilityInject(IIronManCapability.class)
	public static final Capability<IIronManCapability> IRON_MAN_CAP = null;

	public static final UUID damageReductionUUID = UUID.fromString("a03154ce-69de-4cb1-a71f-0a70a4978dc2");

	protected EntityPlayer player;
	protected IronManSuitSetup suit = IronManSuitSetup.EMPTY_SETUP;

	protected int flyingTicks = 0;
	protected boolean repulsor = false;
	protected int repulsorCooldown = 0;
	public static final int maxRepulsorCooldown = 3 * 20;

	public CapabilityIronMan(EntityPlayer player)
	{
		this.player = player;
	}

	@Override
	public void setIronManSuitSetup(IronManSuitSetup suit)
	{
		this.suit = suit;
		this.sync();
	}

	@Override
	public IronManSuitSetup getIronManSuitSetup()
	{
		return suit;
	}

	public void sync()
	{
		IMPacketDispatcher.sendToAll(new MessageSyncIronManCapability(player));
	}

	@Override
	public void exitSuit()
	{
		if (!getIronManSuitSetup().isEmpty() && !player.world.isRemote)
		{
			EntityIronMan entity = new EntityIronMan(player.world, getIronManSuitSetup());
			entity.setPosition(player.posX, player.posY, player.posZ);
			Vec3d lookVec = player.getLookVec();
			entity.motionX = lookVec.x / 2D;
			entity.motionZ = lookVec.z / 2D;
			entity.rotationPitch = player.rotationPitch;
			entity.prevRotationPitch = player.prevRotationPitch;
			entity.rotationYaw = player.rotationYaw;
			entity.prevRotationYaw = player.prevRotationYaw;
			entity.rotationYawHead = player.rotationYawHead;
			entity.prevRotationYawHead = player.prevRotationYawHead;
			entity.renderYawOffset = player.renderYawOffset;
			entity.prevRenderYawOffset = player.prevRenderYawOffset;
			entity.setOwnerId(player.getPersistentID());
			player.world.spawnEntity(entity);
			setIronManSuitSetup(IronManSuitSetup.EMPTY_SETUP);
		}
	}

	@Override
	public NBTTagCompound writeNBT()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		NBTTagCompound suitTag = suit.serializeNBT();
		nbt.setTag("SuitSetup", suitTag);
		nbt.setInteger("FlyingTicks", flyingTicks);
		nbt.setBoolean("Repulsor", repulsor);
		nbt.setInteger("RepulsorCooldown", repulsorCooldown);
		return nbt;
	}

	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		this.suit = new IronManSuitSetup(nbt.getCompoundTag("SuitSetup"));
		this.flyingTicks = nbt.getInteger("FlyingTicks");
		this.repulsor = nbt.getBoolean("Repulsor");
		this.repulsorCooldown = nbt.getInteger("RepulsorCooldown");
	}

	@Override
	public void setCapabilityOwner(EntityPlayer player)
	{
		this.player = player;
		sync();
	}

	@Override
	public int getFlyingTicks()
	{
		return this.flyingTicks;
	}

	@Override
	public void setFlyingTicks(int flyingTicks)
	{
		this.flyingTicks = flyingTicks;
	}

	@Override
	public boolean isRepulsorOn()
	{
		return repulsor;
	}

	@Override
	public void setRepulsorOn(boolean repulsor)
	{
		if (this.repulsor != repulsor)
		{
			this.repulsor = repulsor;
			this.sync();
		}
	}

	@Override
	public int getRepulsorCooldown()
	{
		return repulsorCooldown;
	}

	@Override
	public void setRepulsorCooldown(int cooldown)
	{
		if (cooldown != repulsorCooldown)
		{
			this.repulsorCooldown = cooldown;
			this.sync();
		}
	}

	@SubscribeEvent
	public static void onAttachCapabilities(AttachCapabilitiesEvent<Entity> evt)
	{
		if (evt.getObject() instanceof EntityPlayer)
			evt.addCapability(new ResourceLocation(IronMan.MODID, "iron_man_capability"), new CapabilityIronManProvider(new CapabilityIronMan(
					(EntityPlayer) evt.getObject())));
	}

	public static class CapabilityIronManProvider implements ICapabilitySerializable<NBTTagCompound>
	{

		public IIronManCapability instance;

		public CapabilityIronManProvider(IIronManCapability instance)
		{
			this.instance = instance;
		}

		@Override
		public boolean hasCapability(Capability<?> capability, EnumFacing facing)
		{
			return CapabilityIronMan.IRON_MAN_CAP != null && capability == CapabilityIronMan.IRON_MAN_CAP;
		}

		@Override
		public <T> T getCapability(Capability<T> capability, EnumFacing facing)
		{
			return capability == CapabilityIronMan.IRON_MAN_CAP ? CapabilityIronMan.IRON_MAN_CAP.cast(instance) : null;
		}

		@Override
		public NBTTagCompound serializeNBT()
		{
			return (NBTTagCompound) CapabilityIronMan.IRON_MAN_CAP.getStorage().writeNBT(CapabilityIronMan.IRON_MAN_CAP, instance, null);
		}

		@Override
		public void deserializeNBT(NBTTagCompound nbt)
		{
			CapabilityIronMan.IRON_MAN_CAP.getStorage().readNBT(CapabilityIronMan.IRON_MAN_CAP, instance, null, nbt);
		}

	}

	@SubscribeEvent
	public static void onPlayerStartTracking(PlayerEvent.StartTracking e)
	{
		if (e.getTarget() instanceof EntityPlayer && e.getEntityPlayer() instanceof EntityPlayerMP)
		{
			IMPacketDispatcher.sendTo(new MessageSyncIronManCapability((EntityPlayer) e.getTarget()), (EntityPlayerMP) e.getEntityPlayer());
		}
	}

	@SubscribeEvent
	public static void onEntityJoinWorld(EntityJoinWorldEvent e)
	{
		if (e.getEntity() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) e.getEntity();
			IIronManCapability data = player.getCapability(IRON_MAN_CAP, null);
			data.setCapabilityOwner(player);
		}
	}

	@SubscribeEvent
	public static void onPlayerClone(PlayerEvent.Clone e)
	{
		NBTTagCompound compound = new NBTTagCompound();
		compound = (NBTTagCompound) IRON_MAN_CAP.getStorage().writeNBT(IRON_MAN_CAP, e.getOriginal().getCapability(IRON_MAN_CAP, null), null);
		IRON_MAN_CAP.getStorage().readNBT(IRON_MAN_CAP, e.getEntityPlayer().getCapability(IRON_MAN_CAP, null), null, compound);
	}

	@SubscribeEvent
	public static void onPlayerInteract(PlayerEmptyClickEvent.RightClick e)
	{
		IIronManCapability data = e.getEntityPlayer().getCapability(IRON_MAN_CAP, null);

		if (data != null)
		{

			if (data.getIronManSuitSetup().isEmpty() || !data.getIronManSuitSetup().isEnabled() || e.getEntityPlayer().world.isRemote
					|| e.getHand() == EnumHand.OFF_HAND || data.getIronManSuitSetup()
					.getPart(e.getEntityPlayer().getPrimaryHand() == EnumHandSide.RIGHT ? IronManSuitPart.RIGHT_ARM : IronManSuitPart.LEFT_ARM).isEmpty())
				return;

			if (data.isRepulsorOn())
			{
				data.setRepulsorOn(false);
				PlayerHelper.playSoundToAll(e.getEntityPlayer().world, e.getEntityPlayer().posX, e.getEntityPlayer().posY, e.getEntityPlayer().posZ, 50D,
						IMSoundEvents.REPULSOR_OFF, SoundCategory.PLAYERS);
			}
			else
			{
				data.setRepulsorOn(true);
				PlayerHelper.playSoundToAll(e.getEntityPlayer().world, e.getEntityPlayer().posX, e.getEntityPlayer().posY, e.getEntityPlayer().posZ, 50D,
						IMSoundEvents.REPULSOR_ON, SoundCategory.PLAYERS);
			}
		}
	}

	@SubscribeEvent
	public static void onPlayerInteract(PlayerEmptyClickEvent.LeftClick e)
	{
		IIronManCapability data = e.getEntityPlayer().getCapability(IRON_MAN_CAP, null);
		if (data != null)
		{
			if (data.getIronManSuitSetup().isEmpty() || e.getEntityPlayer().world.isRemote || e.getHand() == EnumHand.OFF_HAND)
				return;

			if (data.isRepulsorOn() && data.getRepulsorCooldown() == 0)
			{
				data.getIronManSuitSetup().repulsorShoot(e.getEntityPlayer());
				data.setRepulsorCooldown(maxRepulsorCooldown);
			}
		}
	}

	@SubscribeEvent
	public static void onPlayerTick(PlayerTickEvent e)
	{
		IIronManCapability data = e.player.getCapability(IRON_MAN_CAP, null);

		if (e.player.ticksExisted < 40)
			data.sync();

		if (!IronManUtil.canWearSuit(e.player) && !data.getIronManSuitSetup().isEmpty() && !e.player.world.isRemote)
		{
			data.exitSuit();
		}

		// Attributes
		if (e.player.getEntityAttribute(SharedMonsterAttributes.ARMOR).getModifier(damageReductionUUID) != null)
			e.player.getEntityAttribute(SharedMonsterAttributes.ARMOR).removeModifier(damageReductionUUID);
		if (!data.getIronManSuitSetup().isEmpty() && data.getIronManSuitSetup().getDamageReduction() > 0F)
			e.player.getEntityAttribute(SharedMonsterAttributes.ARMOR)
					.applyModifier(new AttributeModifier(damageReductionUUID, "iron_man_damage_reduction", data.getIronManSuitSetup().getDamageReduction(), 0));

		// Suit Update
		data.getIronManSuitSetup().onUpdate();

		// Flying
		if (!data.getIronManSuitSetup().isEmpty() && data.getIronManSuitSetup().isFlyingEnabled())
			data.getIronManSuitSetup().handleFlight(e.player);

		if (KeySyncHandler.isFlyKeyDown(e.player))
		{
			data.setFlyingTicks(data.getFlyingTicks() + 1);

			if (data.isRepulsorOn() && data.getIronManSuitSetup().isFlyingEnabled())
			{
				data.setRepulsorOn(false);
				PlayerHelper
						.playSoundToAll(e.player.world, e.player.posX, e.player.posY, e.player.posZ, 50D, IMSoundEvents.REPULSOR_OFF, SoundCategory.PLAYERS);
			}
		}
		else
			data.setFlyingTicks(0);

		// Repulsor
		if (data.isRepulsorOn() && !data.getIronManSuitSetup().isEnabled())
			data.setRepulsorOn(false);
		if (data.isRepulsorOn() && (data.getIronManSuitSetup().isEmpty() || data.getIronManSuitSetup()
				.getPart(e.player.getPrimaryHand() == EnumHandSide.RIGHT ? IronManSuitPart.RIGHT_ARM : IronManSuitPart.LEFT_ARM).isEmpty()))
		{
			data.setRepulsorOn(false);
		}

		if (data.getRepulsorCooldown() > 0)
			data.setRepulsorCooldown(data.getRepulsorCooldown() - 1);

		// Update
		if (data.getIronManSuitSetup().update)
		{
			data.getIronManSuitSetup().update = false;
			data.sync();
		}
	}

	public static class IronManCapabilityStorage implements IStorage<IIronManCapability>
	{

		@Override
		public NBTBase writeNBT(Capability<IIronManCapability> capability, IIronManCapability instance, EnumFacing side)
		{
			return instance.writeNBT();
		}

		@Override
		public void readNBT(Capability<IIronManCapability> capability, IIronManCapability instance, EnumFacing side, NBTBase nbt)
		{
			instance.readNBT((NBTTagCompound) nbt);
		}

	}

}
