package lucraft.mods.heroes.ironman.capabilities;

import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

public interface IIronManCapability {

	public void setCapabilityOwner(EntityPlayer player);
	
	public void setIronManSuitSetup(IronManSuitSetup suit);
	public IronManSuitSetup getIronManSuitSetup();
	
	public void exitSuit();
	public int getFlyingTicks();
	public void setFlyingTicks(int flyingTicks);
	public boolean isRepulsorOn();
	public void setRepulsorOn(boolean repulsor);
	public int getRepulsorCooldown();
	public void setRepulsorCooldown(int cooldown);
	
	public NBTTagCompound writeNBT();
	public void readNBT(NBTTagCompound nbt);
	public void sync();

}
