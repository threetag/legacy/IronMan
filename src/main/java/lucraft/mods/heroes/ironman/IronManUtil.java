package lucraft.mods.heroes.ironman;

import lucraft.mods.heroes.ironman.items.ItemSuitPart;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPart;
import lucraft.mods.heroes.ironman.suitsets.IMSuitSets;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.energy.CapabilityEnergy;

public class IronManUtil {

	public static boolean canWearSuit(EntityPlayer player) {
		return SuitSet.getSuitSet(player) == IMSuitSets.SENSOR_SUIT;
	}

	public static boolean isValidSuitPart(ItemStack stack, IronManSuitPart part) {
		if(stack.isEmpty() || !(stack.getItem() instanceof ItemSuitPart))
			return false;
		
		return ((ItemSuitPart)stack.getItem()).part == part;
	}

	public static boolean isValidReactor(ItemStack stack) {
		return stack.hasCapability(CapabilityEnergy.ENERGY, null);
	}
	
	public static Vec3d rotateZ(Vec3d vector, double angle) {
		float x1 = (float) (vector.x * Math.cos(angle) - vector.y * Math.sin(angle));
		float y1 = (float) (vector.x * Math.sin(angle) + vector.y * Math.cos(angle));
		return new Vec3d(x1, y1, vector.z);
	}
	
}
