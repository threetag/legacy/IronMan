package lucraft.mods.heroes.ironman.network;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.client.gui.MessageIMSendInfoToServer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class IMPacketDispatcher {

	private static byte packetId = 0;

	private static final SimpleNetworkWrapper dispatcher = NetworkRegistry.INSTANCE.newSimpleChannel(IronMan.MODID);

	public static final void registerPackets() {
		IMPacketDispatcher.registerMessage(MessageSyncIronManCapability.Handler.class, MessageSyncIronManCapability.class, Side.CLIENT);
		IMPacketDispatcher.registerMessage(MessageSyncKeys.Handler.class, MessageSyncKeys.class, Side.SERVER);
		IMPacketDispatcher.registerMessage(MessageSuitConstructorButton.Handler.class, MessageSuitConstructorButton.class, Side.SERVER);
		IMPacketDispatcher.registerMessage(MessageSyncEntity.Handler.class, MessageSyncEntity.class, Side.CLIENT);
		IMPacketDispatcher.registerMessage(MessageIMSendInfoToServer.Handler.class, MessageIMSendInfoToServer.class, Side.SERVER);
		IMPacketDispatcher.registerMessage(MessageSyncIronManSuit.Handler.class, MessageSyncIronManSuit.class, Side.SERVER);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static final void registerMessage(Class handlerClass, Class messageClass, Side side) {
		IMPacketDispatcher.dispatcher.registerMessage(handlerClass, messageClass, packetId++, side);
	}

	public static final void sendTo(IMessage message, EntityPlayerMP player) {
		IMPacketDispatcher.dispatcher.sendTo(message, player);
	}

	public static final void sendToAllAround(IMessage message, NetworkRegistry.TargetPoint point) {
		IMPacketDispatcher.dispatcher.sendToAllAround(message, point);
	}

	public static final void sendToAll(IMessage message) {
		IMPacketDispatcher.dispatcher.sendToAll(message);
	}

	public static final void sendToAllAround(IMessage message, int dimension, double x, double y, double z, double range) {
		IMPacketDispatcher.sendToAllAround(message, new NetworkRegistry.TargetPoint(dimension, x, y, z, range));
	}

	public static final void sendToAllAround(IMessage message, EntityPlayer player, double range) {
		IMPacketDispatcher.sendToAllAround(message, player.world.provider.getDimension(), player.posX, player.posY, player.posZ, range);
	}

	public static final void sendToDimension(IMessage message, int dimensionId) {
		IMPacketDispatcher.dispatcher.sendToDimension(message, dimensionId);
	}

	public static final void sendToServer(IMessage message) {
		IMPacketDispatcher.dispatcher.sendToServer(message);
	}
}