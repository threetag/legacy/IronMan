package lucraft.mods.heroes.ironman.network;

import java.util.UUID;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractClientMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSyncIronManCapability implements IMessage {

	private UUID playerUUID;
	private NBTTagCompound data;

	public MessageSyncIronManCapability() {
		
	}

	public MessageSyncIronManCapability(EntityPlayer player) {
		playerUUID = player.getUniqueID();
		data = (NBTTagCompound) CapabilityIronMan.IRON_MAN_CAP.getStorage().writeNBT(CapabilityIronMan.IRON_MAN_CAP, player.getCapability(CapabilityIronMan.IRON_MAN_CAP, null), null);
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		playerUUID = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		data = ByteBufUtils.readTag(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, playerUUID.toString());
		ByteBufUtils.writeTag(buf, data);
	}

	public static class Handler extends AbstractClientMessageHandler<MessageSyncIronManCapability> {
		
		@Override
		public IMessage handleClientMessage(final EntityPlayer player, final MessageSyncIronManCapability message, final MessageContext ctx) {
			
			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {
				
				@Override
				public void run() {
					if(message != null && ctx != null) {
						EntityPlayer en = LucraftCore.proxy.getPlayerEntity(ctx).world.getPlayerEntityByUUID(message.playerUUID);
						
						if(en != null) {
							CapabilityIronMan.IRON_MAN_CAP.getStorage().readNBT(CapabilityIronMan.IRON_MAN_CAP, en.getCapability(CapabilityIronMan.IRON_MAN_CAP, null), null, message.data);
						}
					}
				}
			});

			return null;
		}
	}
	
}
