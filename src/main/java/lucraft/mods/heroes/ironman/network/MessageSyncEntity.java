package lucraft.mods.heroes.ironman.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.ironman.entities.EntityIronMan;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractClientMessageHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSyncEntity implements IMessage {

	public int entityId;
	public NBTTagCompound nbt;
	
	public MessageSyncEntity() {
		
	}
	
	public MessageSyncEntity(EntityIronMan entity) {
		this.entityId = entity.getEntityId();
		this.nbt = new NBTTagCompound();
		entity.writeEntityToNBT(this.nbt);
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.entityId = buf.readInt();
		this.nbt = ByteBufUtils.readTag(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.entityId);
		ByteBufUtils.writeTag(buf, this.nbt);
	}
	
	public static class Handler extends AbstractClientMessageHandler<MessageSyncEntity> {

		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSyncEntity message, MessageContext ctx) {
			
			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {
				
				@Override
				public void run() {
					if(message != null && ctx != null) {
						Entity entity = LucraftCore.proxy.getPlayerEntity(ctx).world.getEntityByID(message.entityId);
						
						if(entity != null && entity instanceof EntityIronMan) {
							((EntityIronMan)entity).readEntityFromNBT(message.nbt);
						}
					}
				}
			});
			
			return null;
		}
		
	}

}
