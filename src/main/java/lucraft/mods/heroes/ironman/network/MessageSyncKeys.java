package lucraft.mods.heroes.ironman.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.ironman.handler.KeySyncHandler;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSyncKeys implements IMessage {

	public boolean flyState;
	public boolean rightClick;

	public MessageSyncKeys() {
	}

	public MessageSyncKeys(boolean flyState, boolean rightClick) {
		this.flyState = flyState;
		this.rightClick = rightClick;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.flyState = buf.readBoolean();
		this.rightClick = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeBoolean(this.flyState);
		buf.writeBoolean(this.rightClick);
	}
	
	public static class Handler extends AbstractServerMessageHandler<MessageSyncKeys> {
		
		@Override
		public IMessage handleServerMessage(final EntityPlayer player, final MessageSyncKeys message, final MessageContext ctx) {
			
			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

				@Override
				public void run() {
					KeySyncHandler.processKeyUpdate(player, message.flyState, message.rightClick);
				}
				
			});
			
			return null;
		}
		
	}

}
