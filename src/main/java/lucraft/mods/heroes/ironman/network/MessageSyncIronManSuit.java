package lucraft.mods.heroes.ironman.network;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.ironman.IronManRegistries;
import lucraft.mods.heroes.ironman.suits.IronManSuit;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractClientMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSyncIronManSuit implements IMessage {

    public IronManSuit suit;
    public JsonObject json;

    public MessageSyncIronManSuit() {
    }

    public MessageSyncIronManSuit(IronManSuit superpower, JsonObject json) {
        this.suit = superpower;
        this.json = json;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.suit = (IronManSuit) ByteBufUtils.readRegistryEntry(buf, IronManRegistries.IRON_MAN_SUIT_REGISTRY);
        this.json = (new JsonParser()).parse(ByteBufUtils.readUTF8String(buf)).getAsJsonObject();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeRegistryEntry(buf, suit);
        ByteBufUtils.writeUTF8String(buf, json.toString());
    }

    public static class Handler extends AbstractClientMessageHandler<MessageSyncIronManSuit> {

        @Override
        public IMessage handleClientMessage(EntityPlayer player, MessageSyncIronManSuit message, MessageContext ctx) {

            LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

                @Override
                public void run() {
                    try {
                        message.suit.deserialize(message.json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });

            return null;
        }

    }

}
