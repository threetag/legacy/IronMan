package lucraft.mods.heroes.ironman.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.ironman.tileentities.TileEntitySuitConstructor;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSuitConstructorButton implements IMessage {

	public BlockPos pos;
	
	public MessageSuitConstructorButton() {
		this.pos = BlockPos.ORIGIN;
	}
	
	public MessageSuitConstructorButton(BlockPos pos) {
		this.pos = pos;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.pos = new BlockPos(buf.readInt(), buf.readInt(), buf.readInt());
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(pos.getX());
		buf.writeInt(pos.getY());
		buf.writeInt(pos.getZ());
	}

	public static class Handler extends AbstractServerMessageHandler<MessageSuitConstructorButton> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageSuitConstructorButton message, MessageContext ctx) {
			
			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

				@Override
				public void run() {
					if(player.world.getTileEntity(message.pos) != null && player.world.getTileEntity(message.pos) instanceof TileEntitySuitConstructor && ((TileEntitySuitConstructor)player.world.getTileEntity(message.pos)).isUsableByPlayer(player)) {
						((TileEntitySuitConstructor)player.world.getTileEntity(message.pos)).build(player);
					}
				}
				
			});
			
			return null;
		}
		
	}
	
}
