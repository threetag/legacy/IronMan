package lucraft.mods.heroes.ironman.suitsets;

import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan;
import lucraft.mods.heroes.ironman.client.models.ModelInvisibleBiped;
import lucraft.mods.heroes.ironman.items.ItemIMSuitSetArmor;
import lucraft.mods.heroes.ironman.suitsets.IMSuitSets.IMSuitSet;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SuitSetSensorSuit extends IMSuitSet {

	public SuitSetSensorSuit(String name) {
		super(name);
	}
	
	@Override
	public void registerItems(Register<Item> e) {
		e.getRegistry().register(this.chestplate = new ItemIMSuitSetArmor(this, EntityEquipmentSlot.CHEST));
		e.getRegistry().register(this.legs = new ItemIMSuitSetArmor(this, EntityEquipmentSlot.LEGS));
		e.getRegistry().register(this.boots = new ItemIMSuitSetArmor(this, EntityEquipmentSlot.FEET));
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public ModelBiped getArmorModel(SuitSet suitSet, ItemStack stack, Entity entity, EntityEquipmentSlot slot, boolean light, boolean smallArms, boolean helmetOpen) {
		if(entity instanceof EntityPlayer && !entity.getCapability(CapabilityIronMan.IRON_MAN_CAP, null).getIronManSuitSetup().isEmpty())
			return new ModelInvisibleBiped();
		return super.getArmorModel(suitSet, stack, entity, slot, light, smallArms, helmetOpen);
	}
}
