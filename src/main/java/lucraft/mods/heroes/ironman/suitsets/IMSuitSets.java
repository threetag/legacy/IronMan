package lucraft.mods.heroes.ironman.suitsets;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.abilities.AbilityExitSuit;
import lucraft.mods.heroes.ironman.abilities.AbilityFaceplate;
import lucraft.mods.heroes.ironman.abilities.AbilityToggleFlight;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

public class IMSuitSets {

	public static List<IMSuitSet> SUIT_SETS = new ArrayList<IMSuitSet>();
	public static ArmorMaterial SENSOR_SUIT_MATERIAL = EnumHelper.addArmorMaterial("sensor_suit", "", 7, new int[]{0, 0, 0, 0}, 0, SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0);
	
	public static IMSuitSet SENSOR_SUIT = new SuitSetSensorSuit("sensor_suit");
	
	public static void init() {
		SUIT_SETS.add(SENSOR_SUIT);
	}
	
	public static class IMSuitSet extends SuitSet {
		
		public IMSuitSet(String name) {
			super(name);
			this.setRegistryName(name);
		}

		@Override
		public ArmorMaterial getArmorMaterial(EntityEquipmentSlot slot) {
			return SENSOR_SUIT_MATERIAL;
		}
		
		@Override
		public CreativeTabs getCreativeTab() {
			return IronMan.TAB_IRON_MAN;
		}

		@Override
		public String getDisplayNameForItem(Item item, ItemStack stack, EntityEquipmentSlot armorType, String origName) {
			String name = StringHelper.translateToLocal("ironman.suit." + getUnlocalizedName() + ".armor");
			name = name.replace("%NAME", getDisplayName()).replace("%PART", StringHelper.translateToLocal(getSuffix(item, stack, armorType)));
			return name;
		}
		
		public String getSuffix(Item item, ItemStack stack, EntityEquipmentSlot armorType) {
			String suffix = "ironman.info.helmet_suffix";
			if (armorType == EntityEquipmentSlot.CHEST)
				suffix = "ironman.info.chestpiece_suffix";
			else if (armorType == EntityEquipmentSlot.LEGS)
				suffix = "ironman.info.legs_suffix";
			else if (armorType == EntityEquipmentSlot.FEET)
				suffix = "ironman.info.boots_suffix";
			return suffix;
		}

		@SideOnly(Side.CLIENT)
		public void registerModels() {
			if(helmet != null)
				ModelLoader.setCustomModelResourceLocation(getHelmet(), 0, new ModelResourceLocation(IronMan.MODID + ":" + getRegistryName().getPath() + "_suit", "helmet"));
			if(chestplate != null)
				ModelLoader.setCustomModelResourceLocation(getChestplate(), 0, new ModelResourceLocation(IronMan.MODID + ":" + getRegistryName().getPath() + "_suit", "chestplate"));
			if(legs != null)
				ModelLoader.setCustomModelResourceLocation(getLegs(), 0, new ModelResourceLocation(IronMan.MODID + ":" + getRegistryName().getPath() + "_suit", "legs"));
			if(boots != null)
				ModelLoader.setCustomModelResourceLocation(getBoots(), 0, new ModelResourceLocation(IronMan.MODID + ":" + getRegistryName().getPath() + "_suit", "boots"));
		}

		@Override public Ability.AbilityMap addDefaultAbilities(EntityLivingBase entity, Ability.AbilityMap abilities, Ability.EnumAbilityContext context)
		{
			abilities.put("exit_suit", new AbilityExitSuit(entity));
			abilities.put("open_faceplate", new AbilityFaceplate(entity));
			abilities.put("toggle_flight", new AbilityToggleFlight(entity));
			return super.addDefaultAbilities(entity, abilities, context);
		}
		
	}
	
}
