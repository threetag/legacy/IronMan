package lucraft.mods.heroes.ironman.client.gui;

import java.util.HashMap;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.ironman.entities.EntityIronMan;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageIMSendInfoToServer implements IMessage {

	public IMServerInfoType type;
	public int info;

	public MessageIMSendInfoToServer() {
	}

	public MessageIMSendInfoToServer(IMServerInfoType type) {
		this.type = type;
		this.info = 0;
	}

	public MessageIMSendInfoToServer(IMServerInfoType type, int i) {
		this.type = type;
		this.info = i;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		type = MessageIMSendInfoToServer.IMServerInfoType.getInfoTypeFromId(buf.readInt());
		info = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(type.ordinal());
		buf.writeInt(info);
	}

	public static class Handler extends AbstractServerMessageHandler<MessageIMSendInfoToServer> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageIMSendInfoToServer message, MessageContext ctx) {
			IMServerInfoType type = message.type;

			switch (type) {
			case TOGGLE_SUIT: {
				Entity entity = LucraftCore.proxy.getPlayerEntity(ctx).world.getEntityByID(message.info);

				if (entity != null && entity instanceof EntityIronMan) {
					((EntityIronMan) entity).getIronManSuitSetup().setEnabled(!((EntityIronMan) entity).getIronManSuitSetup().isEnabled());
					((EntityIronMan) entity).getIronManSuitSetup().markDirty();
				}
			}
				break;

			case DISMANTLE_SUIT: {
				Entity entity = LucraftCore.proxy.getPlayerEntity(ctx).world.getEntityByID(message.info);

				if (entity != null && entity instanceof EntityIronMan && !entity.isDead) {
					EntityIronMan ironman = (EntityIronMan) entity;

					for (ItemStack s : ironman.getIronManSuitSetup().getItems()) {
						ironman.entityDropItem(s, 0.2F);
					}
					ironman.setDead();
				}
			}
				break;

			default:
				break;
			}

			return null;
		}
	}

	public static HashMap<Integer, IMServerInfoType> ids = new HashMap<Integer, MessageIMSendInfoToServer.IMServerInfoType>();

	public enum IMServerInfoType {

		TOGGLE_SUIT, DISMANTLE_SUIT;

		private IMServerInfoType() {
			MessageIMSendInfoToServer.ids.put(this.ordinal(), this);
		}

		public static IMServerInfoType getInfoTypeFromId(int id) {
			return MessageIMSendInfoToServer.ids.get(id);
		}

	}

}
