package lucraft.mods.heroes.ironman.client.gui;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.client.gui.MessageIMSendInfoToServer.IMServerInfoType;
import lucraft.mods.heroes.ironman.container.ContainerSuitInfo;
import lucraft.mods.heroes.ironman.entities.EntityIronMan;
import lucraft.mods.heroes.ironman.network.IMPacketDispatcher;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiButtonExt;

import java.io.IOException;

public class GuiSuitInfo extends GuiContainer {

	private static final ResourceLocation SUIT_INFO_GUI_TEXTURES = new ResourceLocation(IronMan.MODID, "textures/gui/suit_info.png");
	
	public EntityIronMan entity;
	
	public GuiSuitInfo(EntityPlayer player, EntityIronMan entity) {
		super(new ContainerSuitInfo(player, entity));
		this.entity = entity;
		this.xSize = 256;
		this.ySize = 198;
	}

	@Override
	public void initGui() {
		super.initGui();
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
		
		this.buttonList.add(new GuiButtonExt(0, i + 130, j + 20, 80, 20, StringHelper.translateToLocal("ironman.info.system").replace("%s", StringHelper.translateToLocal(entity.getIronManSuitSetup().isEnabled() ? "ironman.info.on" : "ironman.info.off"))));
		this.buttonList.add(new GuiButtonExt(1, i + 130, j + 45, 80, 20, StringHelper.translateToLocal("ironman.info.dismantle")));
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		
		if(button.id == 0) {
			IMPacketDispatcher.sendToServer(new MessageIMSendInfoToServer(IMServerInfoType.TOGGLE_SUIT, entity.getEntityId()));
			this.buttonList.get(0).displayString = StringHelper.translateToLocal("ironman.info.system").replace("%s", StringHelper.translateToLocal(!entity.getIronManSuitSetup().isEnabled() ? "ironman.info.on" : "ironman.info.off"));
		} else if(button.id == 1) {
			IMPacketDispatcher.sendToServer(new MessageIMSendInfoToServer(IMServerInfoType.DISMANTLE_SUIT, entity.getEntityId()));
			mc.player.closeScreen();
		}
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(SUIT_INFO_GUI_TEXTURES);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);
	}

}
