package lucraft.mods.heroes.ironman.client.gui;

import lucraft.mods.heroes.ironman.container.ContainerArcReactor;
import lucraft.mods.heroes.ironman.container.ContainerSuitConstructor;
import lucraft.mods.heroes.ironman.container.ContainerSuitInfo;
import lucraft.mods.heroes.ironman.entities.EntityIronMan;
import lucraft.mods.heroes.ironman.items.InventoryArcReactor;
import lucraft.mods.heroes.ironman.tileentities.TileEntitySuitConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class IMGuiHandler implements IGuiHandler {

	private static int GUI_ID = 0;
	
	public static final int SUIT_CONSTRUCTOR_GUI_ID = GUI_ID++;
	public static final int ARC_REACTOR_GUI_ID = GUI_ID++;
	public static final int SUIT_INFO_GUI_ID = GUI_ID++;
	
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity entity = world.getTileEntity(new BlockPos(x, y, z));
		
		if(ID == SUIT_CONSTRUCTOR_GUI_ID)
			return new ContainerSuitConstructor(player, (TileEntitySuitConstructor) entity);
		
		if(ID == ARC_REACTOR_GUI_ID)
			return new ContainerArcReactor(player, new InventoryArcReactor(player.getHeldItemMainhand()));
		
		if(ID == SUIT_INFO_GUI_ID)
			return new ContainerSuitInfo(player, (EntityIronMan) world.getEntityByID(z));
		
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity entity = world.getTileEntity(new BlockPos(x, y, z));
		
		if(ID == SUIT_CONSTRUCTOR_GUI_ID)
			return new GuiSuitConstructor(player, (TileEntitySuitConstructor) entity);
		
		if(ID == ARC_REACTOR_GUI_ID)
			return new GuiArcReactor(player, new InventoryArcReactor(player.getHeldItemMainhand()));
		
		if(ID == SUIT_INFO_GUI_ID)
			return new GuiSuitInfo(player, (EntityIronMan) world.getEntityByID(z));
		
		return null;
	}

}
