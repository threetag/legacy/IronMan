package lucraft.mods.heroes.ironman.client.gui;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.container.ContainerArcReactor;
import lucraft.mods.heroes.ironman.items.InventoryArcReactor;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Slot;
import net.minecraft.util.ResourceLocation;

public class GuiArcReactor extends GuiContainer {

	private static final ResourceLocation ARC_REACTOR_GUI_TEXTURES = new ResourceLocation(IronMan.MODID, "textures/gui/arc_reactor.png");
	private final EntityPlayer player;
	
	public GuiArcReactor(EntityPlayer player, InventoryArcReactor inventory) {
		super(new ContainerArcReactor(player, inventory));
		this.player = player;
		this.ySize = 223;
	}

	@Override
	protected void handleMouseClick(Slot slotIn, int slotId, int mouseButton, ClickType type) {
		super.handleMouseClick(slotIn, slotId, mouseButton, type);
	}
	
	@Override
	public void initGui() {
		super.initGui();
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(ARC_REACTOR_GUI_TEXTURES);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);
	}

}
