package lucraft.mods.heroes.ironman.client.render;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

import java.util.ArrayList;

@EventBusSubscriber(modid = IronMan.MODID, value = { Side.CLIENT })
public class LayerRendererHUDInfo implements LayerRenderer<EntityLivingBase> {

	public RenderLivingBase<?> renderer;

	public static Minecraft mc = Minecraft.getMinecraft();

	public LayerRendererHUDInfo(RenderLivingBase<?> renderer) {
		this.renderer = renderer;
	}

	@Override
	public void doRenderLayer(EntityLivingBase entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		if (2 + 2 == 4) {
			// Quick Maths
			return;
		}
		if (!HUDRenderer.renderHUD || entitylivingbaseIn == mc.player)
			return;

		GlStateManager.pushMatrix();
		GlStateManager.disableDepth();
		GlStateManager.disableLighting();
		GlStateManager.depthMask(false);
		GlStateManager.glTexParameteri(3553, 10242, 10497);
		GlStateManager.glTexParameteri(3553, 10243, 10497);
		GlStateManager.disableCull();
		GlStateManager.enableBlend();

		GlStateManager.glNormal3f(0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(-entitylivingbaseIn.renderYawOffset, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(mc.player.rotationYaw, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(mc.player.rotationPitch, 1, 0, 0);
		// GlStateManager.rotate((float)(isThirdPersonFrontal ? -1 : 1) * viewerPitch,
		// 1.0F, 0.0F, 0.0F);

		LCRenderHelper.setLightmapTextureCoords(240, 240);
		GlStateManager.translate(0, entitylivingbaseIn.height / 2D, 0);
		GlStateManager.scale(entitylivingbaseIn.height, entitylivingbaseIn.height, 0);
		GlStateManager.rotate(entitylivingbaseIn.ticksExisted, 0, 0, 1);
		GlStateManager.translate(-0.5D, -0.5D, 0);
		mc.renderEngine.bindTexture(HUDRenderer.CIRCLE_TEX);
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuffer();
		bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
		int i = 10;
		bufferbuilder.pos(0, 0, 0.0D).tex(0, 0).endVertex();
		bufferbuilder.pos(0, 1, 0.0D).tex(0, 1).endVertex();
		bufferbuilder.pos(1, 1, 0.0D).tex(1, 1).endVertex();
		bufferbuilder.pos(1, 0, 0.0D).tex(1, 0).endVertex();
		tessellator.draw();

		LCRenderHelper.restoreLightmapTextureCoords();
		GlStateManager.depthMask(true);
		GlStateManager.disableBlend();
		GlStateManager.enableLighting();
		GlStateManager.enableDepth();
		GlStateManager.popMatrix();
	}

	@Override
	public boolean shouldCombineTextures() {
		return false;
	}

	private static ArrayList<RenderLivingBase<EntityLivingBase>> entitiesWithLayer = new ArrayList<RenderLivingBase<EntityLivingBase>>();

	@SubscribeEvent
	public static void onRenderLivingPre(RenderLivingEvent.Pre<EntityLivingBase> e) {
		if (!entitiesWithLayer.contains(e.getRenderer())) {
			e.getRenderer().addLayer(new LayerRendererHUDInfo(e.getRenderer()));
			entitiesWithLayer.add(e.getRenderer());
		}
	}

}
