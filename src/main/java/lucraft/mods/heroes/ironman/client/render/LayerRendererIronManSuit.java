package lucraft.mods.heroes.ironman.client.render;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan;
import lucraft.mods.heroes.ironman.capabilities.IIronManCapability;
import lucraft.mods.heroes.ironman.client.models.ModelIronManSuit;
import lucraft.mods.heroes.ironman.handler.KeySyncHandler;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@EventBusSubscriber(modid = IronMan.MODID, value = { Side.CLIENT })
public class LayerRendererIronManSuit implements LayerRenderer<EntityPlayer> {

	public RenderLivingBase<?> renderer;

	public static Minecraft mc = Minecraft.getMinecraft();

	public LayerRendererIronManSuit(RenderLivingBase<?> renderer) {
		this.renderer = renderer;
	}

	@Override
	public void doRenderLayer(EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		IIronManCapability data = player.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);

		if (data != null && !data.getIronManSuitSetup().isEmpty()) {
			float glow = (float) data.getIronManSuitSetup().getEnabledTimer() / (float) IronManSuitSetup.maxEnabledTimer;
			data.getIronManSuitSetup().renderSuit(player, renderer, (ModelBiped) renderer.getMainModel(), limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, scale, 1, glow);
		}
	}

	@Override
	public boolean shouldCombineTextures() {
		return false;
	}

	@SubscribeEvent
	public static void onSetUpModel(RenderModelEvent.SetRotationAngels e) {
		if (e.getEntity() instanceof EntityPlayer && !e.getEntity().getCapability(CapabilityIronMan.IRON_MAN_CAP, null).getIronManSuitSetup().isEmpty()) {
			EntityPlayer player = (EntityPlayer) e.getEntity();

			if (e.model instanceof ModelPlayer) {
				ModelPlayer model = (ModelPlayer) e.model;
				model.bipedBodyWear.showModel = false;
				model.bipedRightArmwear.showModel = false;
				model.bipedLeftArmwear.showModel = false;
				model.bipedRightLegwear.showModel = false;
				model.bipedLeftLegwear.showModel = false;
				model.bipedHeadwear.showModel = false;
			}

			IIronManCapability data = e.getEntity().getCapability(CapabilityIronMan.IRON_MAN_CAP, null);

			if (data.getIronManSuitSetup().isFlyingEnabled()) {
				double d = Math.min(Math.sqrt((player.prevPosX - player.posX) * (player.prevPosX - player.posX) + (player.prevPosZ - player.posZ) * (player.prevPosZ - player.posZ)), 1.0D) * (player.moveForward == 0.0F ? 1.0F : player.moveForward);
				e.limbSwingAmount = 0;
				if (KeySyncHandler.isFlyKeyDown((EntityLivingBase) e.getEntity()))
					e.netHeadYaw = (float) (-90 * d);
			}

			if (data.isRepulsorOn()) {
				e.setCanceled(true);
				if (player.getPrimaryHand() == EnumHandSide.RIGHT) {
					e.model.bipedRightArm.rotateAngleX = (float) (e.model.bipedHead.rotateAngleX + Math.toDegrees(90F));
					e.model.bipedRightArm.rotateAngleY = e.model.bipedHead.rotateAngleY;
				} else {
					e.model.bipedLeftArm.rotateAngleX = (float) (e.model.bipedHead.rotateAngleX + Math.toDegrees(90F));
					e.model.bipedLeftArm.rotateAngleY = e.model.bipedHead.rotateAngleY;
				}

				if (e.model instanceof ModelIronManSuit) {
					ModelIronManSuit model = (ModelIronManSuit) e.model;
					if (player.getPrimaryHand() == EnumHandSide.RIGHT) {
						model.bipedRightArmwear.rotateAngleX = model.bipedRightArm.rotateAngleX;
						model.bipedRightArmwear.rotateAngleY = model.bipedRightArm.rotateAngleY;
					} else {
						model.bipedLeftArmwear.rotateAngleX = model.bipedLeftArm.rotateAngleX;
						model.bipedLeftArmwear.rotateAngleY = model.bipedLeftArm.rotateAngleY;
					}
				}
			}
		}
	}

	@SubscribeEvent
	public static void onRenderModel(RenderModelEvent e) {
		if (e.getEntity() instanceof EntityPlayer && !e.getEntity().getCapability(CapabilityIronMan.IRON_MAN_CAP, null).getIronManSuitSetup().isEmpty() && e.getEntity().getCapability(CapabilityIronMan.IRON_MAN_CAP, null).getIronManSuitSetup().isFlyingEnabled()) {
			EntityPlayer player = (EntityPlayer) e.getEntityLiving();

			// if (JetpackSyncHandler.isFlyKeyDown((EntityLivingBase)
			// e.getEntity())) {
			// float f =
			// e.getEntityLiving().getCapability(CapabilityIronMan.IRON_MAN_CAP,
			// null).getFlyingTicks() + LucraftCoreClientUtil.renderTick;
			// float f1 = MathHelper.clamp(f * f / 100.0F, 0.0F, 1.0F);
			// GlStateManager.translate(0, f1 * -0.8F, 0);
			// GlStateManager.rotate(f1 * (90.0F + player.rotationPitch), 1.0F,
			// 0.0F, 0.0F);
			// Vec3d vec3d = player.getLook(LucraftCoreClientUtil.renderTick);
			// double d0 = player.motionX * player.motionX + player.motionZ *
			// player.motionZ;
			// double d1 = vec3d.x * vec3d.x + vec3d.z * vec3d.z;
			//
			// if (d0 > 0.0D && d1 > 0.0D) {
			// double d2 = (player.motionX * vec3d.x + player.motionZ * vec3d.z)
			// / (Math.sqrt(d0) * Math.sqrt(d1));
			// double d3 = player.motionX * vec3d.z - player.motionZ * vec3d.x;
			// GlStateManager.rotate((float) (Math.signum(d3) * Math.acos(d2)) *
			// 180.0F / (float) Math.PI, 0.0F, 1.0F, 0.0F);
			// }
			// } else {
			// double d = Math.min(Math.sqrt((player.prevPosX - player.posX) *
			// (player.prevPosX - player.posX) + (player.prevPosZ - player.posZ)
			// * (player.prevPosZ - player.posZ)), 1.0D) * (player.moveForward
			// == 0.0F ? 1.0F : player.moveForward);
			// GL11.glRotated(200.0D * MathHelper.clamp(d, -0.3D, 0.3D), 1.0D,
			// 0.0D, 0.0D);
			// GlStateManager.translate(0, 0, 30 * d * 4 / 100);
			// }
			float speed = (float) MathHelper.clamp(Math.sqrt((player.prevPosX - player.posX) * (player.prevPosX - player.posX) + (player.prevPosZ - player.posZ) * (player.prevPosZ - player.posZ)), 0F, 1F);
			GlStateManager.rotate(speed * (90F + player.rotationPitch), 1, 0, 0);
		}
	}

}
