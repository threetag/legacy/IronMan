package lucraft.mods.heroes.ironman.client.gui;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.container.ContainerSuitConstructor;
import lucraft.mods.heroes.ironman.container.ContainerSuitConstructor.SlotSuitConstructor;
import lucraft.mods.heroes.ironman.network.IMPacketDispatcher;
import lucraft.mods.heroes.ironman.network.MessageSuitConstructorButton;
import lucraft.mods.heroes.ironman.tileentities.TileEntitySuitConstructor;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiButtonExt;

import java.io.IOException;

public class GuiSuitConstructor extends GuiContainer {

	private static final ResourceLocation SUIT_CONSTRUCTOR_GUI_TEXTURES = new ResourceLocation(IronMan.MODID, "textures/gui/suit_constructor.png");
	private final TileEntitySuitConstructor tileEntity;
	private final EntityPlayer player;
	
	public GuiSuitConstructor(EntityPlayer player, TileEntitySuitConstructor tileEntity) {
		super(new ContainerSuitConstructor(player, tileEntity));
		this.tileEntity = tileEntity;
		this.player = player;
		this.ySize = 181;
	}

	@Override
	public void initGui() {
		super.initGui();
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
		
		this.buttonList.add(new GuiButtonExt(0, i + 100, j + 65, 60, 18, StringHelper.translateToLocal("ironman.info.construct")));
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if(button.id == 0) {
			IMPacketDispatcher.sendToServer(new MessageSuitConstructorButton(tileEntity.getPos()));
		}
		super.actionPerformed(button);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}

	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
//		String s = this.tileEntity.getDisplayName().getUnformattedText();
//		this.fontRenderer.drawString(s, this.xSize / 2 - this.fontRenderer.getStringWidth(s) / 2, 6, 4210752);
//		this.fontRenderer.drawString(this.player.inventory.getDisplayName().getUnformattedText(), 8, this.ySize - 96 + 2, 4210752);
		
		if(this.getSlotUnderMouse() != null && this.getSlotUnderMouse() instanceof SlotSuitConstructor) {
			this.drawHoveringText(StringHelper.translateToLocal("item.suit_part_" + ((SlotSuitConstructor)this.getSlotUnderMouse()).part.toString().toLowerCase() + ".name").replaceAll("%SUIT ", ""), 0, 0);
		}
		
		this.buttonList.get(0).enabled = !tileEntity.isEmpty();
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(SUIT_CONSTRUCTOR_GUI_TEXTURES);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);
	}

}