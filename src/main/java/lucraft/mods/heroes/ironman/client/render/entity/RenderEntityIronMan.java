package lucraft.mods.heroes.ironman.client.render.entity;

import lucraft.mods.heroes.ironman.client.models.ModelIronManSuit;
import lucraft.mods.heroes.ironman.entities.EntityIronMan;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerArrow;
import net.minecraft.client.renderer.entity.layers.LayerElytra;
import net.minecraft.client.renderer.entity.layers.LayerHeldItem;
import net.minecraft.util.ResourceLocation;

public class RenderEntityIronMan extends RenderBiped<EntityIronMan> {

	public RenderEntityIronMan(RenderManager renderManagerIn) {
		super(renderManagerIn, new ModelIronManSuit(0F, false), 0.5F);

		// this.addLayer(new LayerBipedArmor(this));
		this.addLayer(new LayerHeldItem(this));
		this.addLayer(new LayerArrow(this));
		this.addLayer(new LayerElytra(this));
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityIronMan entity) {
		return new ResourceLocation("textures/entity/zombie/zombie.png");
	}

	@Override
	protected void renderModel(EntityIronMan entitylivingbaseIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor) {
		boolean flag = this.isVisible(entitylivingbaseIn);
		boolean flag1 = !flag && !entitylivingbaseIn.isInvisibleToPlayer(Minecraft.getMinecraft().player);
		
		if (flag || flag1) {
			// 1st Layer
			if (!this.bindEntityTexture(entitylivingbaseIn))
				return;

			if (flag1)
				GlStateManager.enableBlendProfile(GlStateManager.Profile.TRANSPARENT_MODEL);

			float glow = (float)entitylivingbaseIn.getIronManSuitSetup().getEnabledTimer() / (float)IronManSuitSetup.maxEnabledTimer;
			entitylivingbaseIn.getIronManSuitSetup().renderSuit(entitylivingbaseIn, this, (ModelBiped) this.mainModel, limbSwing, limbSwingAmount, LCRenderHelper.renderTick, ageInTicks, netHeadYaw * glow, headPitch * glow, scaleFactor, 1, glow);

			if (flag1)
				GlStateManager.disableBlendProfile(GlStateManager.Profile.TRANSPARENT_MODEL);
		}
	}
	
	@Override
	protected void preRenderCallback(EntityIronMan entitylivingbaseIn, float partialTickTime) {
		float f = 0.9375F;
		GlStateManager.scale(0.9375F, 0.9375F, 0.9375F);
	}

}
