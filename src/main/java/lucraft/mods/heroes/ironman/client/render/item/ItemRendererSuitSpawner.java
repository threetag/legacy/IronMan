package lucraft.mods.heroes.ironman.client.render.item;

import com.mojang.authlib.GameProfile;
import lucraft.mods.heroes.ironman.client.models.ModelIronManSuit;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntityItemStackRenderer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class ItemRendererSuitSpawner extends TileEntityItemStackRenderer {

    @Override
    public void renderByItem(ItemStack stack, float partialTicks) {
        if(!stack.hasTagCompound())
            return;

        IronManSuitSetup setup = new IronManSuitSetup(stack.getTagCompound());
        Minecraft mc = Minecraft.getMinecraft();
        FakePlayerClient entity = new FakePlayerClient(mc.world, new GameProfile(null, "IRON_MAN_SPAWNER"));
        entity.info = "";

        GlStateManager.translate(0.5F, 0, 0.5F);
        GlStateManager.rotate(180F, 1, 0, 0);
        GL11.glDisable(GL11.GL_CULL_FACE);
        ModelIronManSuit model = new ModelIronManSuit(0, false);
        model.isChild = false;
        model.isSneak = false;
        model.isRiding = false;
        setup.renderSuit((EntityLivingBase) entity, null, model, 0, 0, 0, 0, 0, 0, 0.0625F);
        GL11.glEnable(GL11.GL_CULL_FACE);
        GlStateManager.disableBlend();

    }
}
