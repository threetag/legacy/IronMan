package lucraft.mods.heroes.ironman.client.render.tileentity;

import com.mojang.authlib.GameProfile;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.blocks.BlockSuitConstructor;
import lucraft.mods.heroes.ironman.client.models.ModelIronManSuit;
import lucraft.mods.heroes.ironman.client.models.ModelSuitConstructor;
import lucraft.mods.heroes.ironman.client.render.item.FakePlayerClient;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import lucraft.mods.heroes.ironman.tileentities.TileEntitySuitConstructor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class TileEntityRendererSuitConstructor extends TileEntitySpecialRenderer {

	public static final ModelSuitConstructor MODEL = new ModelSuitConstructor();
	public static final ResourceLocation TEXTURE = new ResourceLocation(IronMan.MODID, "textures/models/suit_constructor.png");

	@Override
	public void render(TileEntity te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
		super.render(te, x, y, z, partialTicks, destroyStage, alpha);
		if (te.getWorld().isAirBlock(te.getPos()))
			return;

		TileEntitySuitConstructor tileEntity = (TileEntitySuitConstructor) te;

		float rotation = 0F;
		EnumFacing facing = te.getWorld().getBlockState(te.getPos()).getValue(BlockSuitConstructor.FACING);
		switch (facing) {
		case SOUTH:
			rotation = 180F;
			break;
		case WEST:
			rotation = 270F;
			break;
		case EAST:
			rotation = 90F;
			break;
		default:
			break;
		}

		GlStateManager.pushMatrix();
		GlStateManager.translate(x + 0.5D, y + 1.5D, z + 0.5D);
		GlStateManager.rotate(180, 0F, 0F, 1F);
		GlStateManager.rotate(rotation, 0, 1, 0);
		this.bindTexture(TEXTURE);
		MODEL.renderModel(0.0625F);

		IronManSuitSetup setup = new IronManSuitSetup(tileEntity.getStackInSlot(0), tileEntity.getStackInSlot(1), tileEntity.getStackInSlot(2), tileEntity.getStackInSlot(3), tileEntity.getStackInSlot(4), tileEntity.getStackInSlot(5), tileEntity.getStackInSlot(6), tileEntity.getStackInSlot(7));

		if (!setup.isEmpty()) {
			ModelIronManSuit model = new ModelIronManSuit(0, false);
			model.isChild = false;
			model.isSneak = false;
			model.isRiding = false;
			Entity entity = new FakePlayerClient(te.getWorld(), new GameProfile(null, "SUIT_CONSTRUCTOR"));
			GlStateManager.translate(0, Math.sin((Minecraft.getMinecraft().player.ticksExisted + partialTicks) / 10F) / 100F, 0);
			GlStateManager.scale(0.7F, 0.7D, 0.7D);
			GlStateManager.enableBlend();
			setup.renderSuit((EntityLivingBase) entity, null, model, 0, 0, 0, 0, 0, 0, 0.0625F, 0.3F, 0.3F);
			GlStateManager.disableBlend();
		}

		GlStateManager.popMatrix();
	}

}
