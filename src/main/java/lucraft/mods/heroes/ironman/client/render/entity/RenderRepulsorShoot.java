package lucraft.mods.heroes.ironman.client.render.entity;

import lucraft.mods.heroes.ironman.entities.EntityRepulsorShoot;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderRepulsorShoot extends Render<EntityRepulsorShoot> {

	public RenderRepulsorShoot(RenderManager renderManager) {
		super(renderManager);
	}
	
	@Override
	public void doRender(EntityRepulsorShoot entity, double x, double y, double z, float entityYaw, float partialTicks) {
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
		
		GlStateManager.pushMatrix();
		GL11.glDisable(3553);
		GL11.glDisable(2896);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GlStateManager.enableBlend();
		LCRenderHelper.setLightmapTextureCoords(240, 240);
		GlStateManager.translate(x, y, z);
		float scale = 0.2F;
		GlStateManager.scale(scale, scale, scale);
		GlStateManager.translate(-0.5D, 0, -0.5D);
		
		Tessellator tes = Tessellator.getInstance();
		BufferBuilder bb = tes.getBuffer();
		
		for(int i = 0; i < 2; i++) {
			GlStateManager.pushMatrix();
			if(i == 1) {
				GlStateManager.color(0.3F, 0.8F, 0.8F, 0.5F);
			} else {
				GlStateManager.color(1, 1, 1, 1);
				float scale_ = 0.8F;
				GlStateManager.scale(scale_, scale_, scale_);
				GlStateManager.translate(0.1F, 0.1F, 0.1F);
			}
			
			bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
			bb.pos(0, 0, 0).endVertex();
			bb.pos(0, 0, 1).endVertex();
			bb.pos(1, 0, 1).endVertex();
			bb.pos(1, 0, 0).endVertex();
			tes.draw();
			
			bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
			bb.pos(0, 1, 0).endVertex();
			bb.pos(0, 1, 1).endVertex();
			bb.pos(1, 1, 1).endVertex();
			bb.pos(1, 1, 0).endVertex();
			tes.draw();
			
			bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
			bb.pos(0, 0, 0).endVertex();
			bb.pos(0, 0, 1).endVertex();
			bb.pos(0, 1, 1).endVertex();
			bb.pos(0, 1, 0).endVertex();
			tes.draw();
			
			bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
			bb.pos(1, 0, 0).endVertex();
			bb.pos(1, 0, 1).endVertex();
			bb.pos(1, 1, 1).endVertex();
			bb.pos(1, 1, 0).endVertex();
			tes.draw();
			
			bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
			bb.pos(0, 0, 0).endVertex();
			bb.pos(1, 0, 0).endVertex();
			bb.pos(1, 1, 0).endVertex();
			bb.pos(0, 1, 0).endVertex();
			tes.draw();
			
			bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
			bb.pos(0, 0, 1).endVertex();
			bb.pos(1, 0, 1).endVertex();
			bb.pos(1, 1, 1).endVertex();
			bb.pos(0, 1, 1).endVertex();
			tes.draw();
			GlStateManager.popMatrix();
		}
		
		GL11.glEnable(3553);
		GL11.glEnable(2896);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GlStateManager.disableBlend();
		LCRenderHelper.restoreLightmapTextureCoords();
		GlStateManager.popMatrix();
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityRepulsorShoot entity) {
		return null;
	}

}
