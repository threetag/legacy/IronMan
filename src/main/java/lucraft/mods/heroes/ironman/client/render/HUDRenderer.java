package lucraft.mods.heroes.ironman.client.render;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan;
import lucraft.mods.heroes.ironman.capabilities.IIronManCapability;
import lucraft.mods.heroes.ironman.items.ItemSuitPart;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPart;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.List;

@EventBusSubscriber(modid = IronMan.MODID, value = { Side.CLIENT })
public class HUDRenderer {

	public static Minecraft mc = Minecraft.getMinecraft();
	
	public static boolean renderHUD = false;
	
	public static final ResourceLocation CIRCLE_TEX = new ResourceLocation(IronMan.MODID, "textures/gui/hud_circle.png");
	public static final ResourceLocation REPULSOR_TEX = new ResourceLocation(IronMan.MODID, "textures/gui/hud_repulsor.png");
	public static final ResourceLocation REPULSOR_OVERLAY_TEX = new ResourceLocation(IronMan.MODID, "textures/gui/hud_repulsor_overlay.png");
	
	@SubscribeEvent
	public static void onRender(RenderGameOverlayEvent.Pre e) {
		if (e.getType() != ElementType.ALL || mc.gameSettings.showDebugInfo || mc.gameSettings.thirdPersonView != 0) {
			return;
		}
		
		IIronManCapability data = mc.player.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);
		IronManSuitSetup suit = data.getIronManSuitSetup();
		
		if(!suit.isEmpty() && suit.isEnabled() && suit.getFaceplateTimer() < IronManSuitSetup.maxFaceplateTimer && !suit.getPart(IronManSuitPart.FACEPLATE).isEmpty() && ItemSuitPart.getIronManSuit(suit.getPart(IronManSuitPart.FACEPLATE)).getSuitPartInfo(IronManSuitPart.FACEPLATE).showHud()) {
			renderHUD = true;
			GlStateManager.pushMatrix();
			GlStateManager.rotate(((float)data.getIronManSuitSetup().getFaceplateTimer() / (float)IronManSuitSetup.maxFaceplateTimer) * 90F, 1, 0, 0);
			ScaledResolution res = e.getResolution();
			Tessellator tes = Tessellator.getInstance();
			BufferBuilder bb = tes.getBuffer();
			int widthCenter = res.getScaledWidth() / 2;
			int heightCenter = res.getScaledHeight() / 2;
			int widthThird = res.getScaledWidth() / 3;
			int heightThird = res.getScaledHeight() / 5;
			int lowerLength = (int) (0.3F * widthCenter);
			int xThickness = (int) (res.getScaledWidth() * 0.1F);
			int yThickness = (int) (res.getScaledHeight() * 0.1F);
			
			// Arc Reactor
			GlStateManager.enableBlend();
			GlStateManager.pushMatrix();
			GlStateManager.color(1, 1, 1, 1);
			GlStateManager.translate(80, res.getScaledHeight() / 1.3D, 0);
			GlStateManager.scale(res.getScaleFactor() / 3D, res.getScaleFactor() / 3D, 0);
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(-48, -48, 0);
			GlStateManager.scale(6, 6, 6);
			ItemStack reactor = suit.getReactor();
			
			if(reactor.isEmpty()) {
				mc.getRenderItem().renderItemIntoGUI(new ItemStack(Blocks.BARRIER), 0, 0);
			} else {
				mc.getRenderItem().renderItemIntoGUI(suit.getReactor(), 0, 0);
			}
			
			GlStateManager.popMatrix();
			
			if(!reactor.isEmpty()) {
				int energy = (int) (((float)suit.getEnergy() / (float)suit.getMaxEnergy()) * 100);
				GlStateManager.pushMatrix();
				double backScale = 1F/(res.getScaleFactor() / 3D);
				GlStateManager.scale(backScale, backScale, backScale);
				String energyString = energy + "%";
				GlStateManager.translate(-(mc.fontRenderer.getStringWidth(energyString) / 2), -3, 0);
				LCRenderHelper.drawStringWithOutline(energyString, 0, 0, 0xffffff, 0);
				GlStateManager.popMatrix();
			}
			
			GlStateManager.rotate(mc.player.ticksExisted + e.getPartialTicks(), 0, 0, 1);
			GlStateManager.translate(-64, -64, 0);
			GlStateManager.scale(0.5D, 0.5D, 0.5D);
			mc.renderEngine.bindTexture(CIRCLE_TEX);
			mc.ingameGUI.drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			GlStateManager.popMatrix();
			
			// Repulsor
			float repulsor = 1F - (float)data.getRepulsorCooldown() / (float)CapabilityIronMan.maxRepulsorCooldown;
			GlStateManager.pushMatrix();
			GlStateManager.color(1, 1, 1, 1);
			GlStateManager.translate(80, res.getScaledHeight() / 5D, 0);
			GlStateManager.scale(res.getScaleFactor() / 3D, res.getScaleFactor() / 3D, 0);
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(-48, -48, 0);
			GlStateManager.scale(0.375F, 0.375F, 0);
			mc.renderEngine.bindTexture(REPULSOR_TEX);
			mc.ingameGUI.drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			mc.renderEngine.bindTexture(new ResourceLocation(IronMan.MODID, "textures/gui/hud_repulsor_overlay.png"));
			mc.ingameGUI.drawTexturedModalRect(0, 0, 0, 0, 256, (int) (256 * repulsor));
			GlStateManager.popMatrix();
			
			GlStateManager.rotate(-mc.player.ticksExisted - e.getPartialTicks(), 0, 0, 1);
			GlStateManager.translate(-64, -64, 0);
			GlStateManager.scale(0.5D, 0.5D, 0);
			mc.renderEngine.bindTexture(CIRCLE_TEX);
			mc.ingameGUI.drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			GlStateManager.popMatrix();
			
			// Edges
//			GlStateManager.color(0.3F, 0.8F, 0.8F, 0.5F);
			GlStateManager.disableTexture2D();
			GL11.glBlendFunc(770, 771);
			GL11.glAlphaFunc(516, 0.003921569F);
			GL11.glDisable(3553);
			GL11.glDisable(2896);
			GL11.glDisable(GL11.GL_CULL_FACE);
			GlStateManager.color(0, 0, 0, 0.7F);
			
			bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
			bb.pos((widthCenter) - (lowerLength * 2), 0, 0).endVertex();
			bb.pos((widthCenter) - lowerLength, yThickness, 0).endVertex();
			bb.pos((widthCenter) + lowerLength, yThickness, 0).endVertex();
			bb.pos((widthCenter) + (lowerLength * 2), 0, 0).endVertex();
			tes.draw();
			
			bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
			bb.pos((widthCenter) - (lowerLength * 2), res.getScaledHeight(), 0).endVertex();
			bb.pos((widthCenter) - lowerLength, res.getScaledHeight() - yThickness, 0).endVertex();
			bb.pos((widthCenter) + lowerLength, res.getScaledHeight() - yThickness, 0).endVertex();
			bb.pos((widthCenter) + (lowerLength * 2), res.getScaledHeight(), 0).endVertex();
			tes.draw();
			
			// Items
			List<ItemStack> items = new ArrayList<>();
			for(IronManSuitPart part : IronManSuitPart.values()) {
				if(!suit.getPart(part).isEmpty())
					items.add(suit.getPart(part));
			}
			
			for(int i = 0; i < items.size(); i++) {
				ItemStack stack = items.get(i);

				GlStateManager.pushMatrix();
				GlStateManager.color(0.3F, 0.9F, 0.9F, 0.5F);
				GlStateManager.disableTexture2D();
				GlStateManager.translate(-4, 4, 0);
				GlStateManager.enableBlend();
				GL11.glDisable(GL11.GL_CULL_FACE);
				bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
				bb.pos(res.getScaledWidth() - 45,   20 + i * 20, 0).endVertex();
				bb.pos(res.getScaledWidth(), 20 + i * 20, 0).endVertex();
				bb.pos(res.getScaledWidth(),  20 + (i * 20) + 18, 0).endVertex();
				bb.pos(res.getScaledWidth() - 45,  20 + (i * 20) + 18, 0).endVertex();
				tes.draw();
				GlStateManager.enableTexture2D();
				GlStateManager.popMatrix();

				mc.getRenderItem().renderItemIntoGUI(stack, res.getScaledWidth() - 20,  24 + i * 20);
				mc.fontRenderer.drawString((int)(((((float)stack.getMaxDamage() - (float)stack.getItemDamage()) / (float)stack.getMaxDamage())) * 100F) + "%", res.getScaledWidth() - 46,  29 + i * 20, 0xffffff);
			}
			
			GL11.glEnable(3553);
			GL11.glEnable(2896);
			GL11.glEnable(GL11.GL_CULL_FACE);
			GlStateManager.disableBlend();
			GlStateManager.popMatrix();
		} else {
			renderHUD = false;
		}
	}
	
}
