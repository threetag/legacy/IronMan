package lucraft.mods.heroes.ironman.client.render.item;

import org.lwjgl.opengl.GL11;

import com.mojang.authlib.GameProfile;

import lucraft.mods.heroes.ironman.client.models.ModelIronManSuit;
import lucraft.mods.heroes.ironman.items.ItemSuitPart;
import lucraft.mods.heroes.ironman.suits.IronManSuit;
import lucraft.mods.lucraftcore.util.entity.FakePlayerClient;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntityItemStackRenderer;
import net.minecraft.item.ItemStack;

public class ItemRendererSuitPart extends TileEntityItemStackRenderer {

	@Override
	public void renderByItem(ItemStack stack, float partialTicks) {
		IronManSuit suit = ItemSuitPart.getIronManSuit(stack);
		if (suit == null)
			return;
		ItemSuitPart item = (ItemSuitPart) stack.getItem();
		Minecraft mc = Minecraft.getMinecraft();

		GlStateManager.translate(0.5F, 0, 0.5F);
		GlStateManager.enableBlend();
		ModelIronManSuit model = new ModelIronManSuit(0, false);
		item.part.setModelVisibilities(model);
		model.isChild = false;
		model.isSneak = false;
		model.isRiding = false;

		// part.setModelVisibilities(model);

		FakePlayerClient entity = new FakePlayerClient(mc.world, new GameProfile(null, "IRON_MAN_ITEM"));
		entity.info = "";

		GlStateManager.rotate(180F, 1, 0, 0);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GlStateManager.enableBlend();
		GL11.glBlendFunc(770, 771);
		GL11.glAlphaFunc(516, 0.003921569F);
		mc.renderEngine.bindTexture(suit.getSuitTexture(false));
		model.render(entity, 0, 0, 0, 0, 0, 0.0625F);

		if (suit.getSuitTexture(true) != null) {
			GlStateManager.disableLighting();
			LCRenderHelper.setLightmapTextureCoords(240, 240);
			mc.renderEngine.bindTexture(suit.getSuitTexture(true));
			model.render(entity, 0, 0, 0, 0, 0, 0.0625F);
			LCRenderHelper.restoreLightmapTextureCoords();
			GlStateManager.enableLighting();
		}

		GL11.glEnable(GL11.GL_CULL_FACE);
		GlStateManager.disableBlend();

	}
}
