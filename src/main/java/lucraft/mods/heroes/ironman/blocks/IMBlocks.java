package lucraft.mods.heroes.ironman.blocks;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.tileentities.TileEntitySuitConstructor;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@EventBusSubscriber(modid = IronMan.MODID)
public class IMBlocks {

	public static Block SUIT_CONSTRUCTOR = new BlockSuitConstructor();
	
	@SubscribeEvent
	public static void onRegisterBlocks(RegistryEvent.Register<Block> e) {
		e.getRegistry().register(SUIT_CONSTRUCTOR);
		
		GameRegistry.registerTileEntity(TileEntitySuitConstructor.class, "suit_constructor");
	}
	
	@SubscribeEvent
	public static void onRegisterItems(RegistryEvent.Register<Item> e) {
		e.getRegistry().register(new ItemBlock(SUIT_CONSTRUCTOR).setRegistryName(SUIT_CONSTRUCTOR.getRegistryName()));
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public static void onRegisterModels(ModelRegistryEvent e) {
		ModelResourceLocation loc = new ModelResourceLocation(new ResourceLocation(IronMan.MODID, "suit_constructor"), "inventory");
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(SUIT_CONSTRUCTOR), 0, loc);
		ModelBakery.registerItemVariants(Item.getItemFromBlock(SUIT_CONSTRUCTOR), loc);
	}
	
}
