package lucraft.mods.heroes.ironman;

import lucraft.mods.heroes.ironman.abilities.AbilityExitSuit;
import lucraft.mods.heroes.ironman.abilities.AbilityFaceplate;
import lucraft.mods.heroes.ironman.abilities.AbilityToggleFlight;
import lucraft.mods.heroes.ironman.suits.IronManSuit;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityEntry;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;

@EventBusSubscriber(modid = IronMan.MODID)
public class IronManRegistries {

	public static IForgeRegistry<IronManSuit> IRON_MAN_SUIT_REGISTRY;
	
	@SubscribeEvent
	public static void onRegisterNewRegistries(RegistryEvent.NewRegistry e) {
		IRON_MAN_SUIT_REGISTRY = new RegistryBuilder<IronManSuit>().setName(new ResourceLocation(IronMan.MODID, "iron_man_suits")).setType(IronManSuit.class).setIDRange(0, 512).create();
	}

	@SubscribeEvent
	public static void onRegisterAbilities(RegistryEvent.Register<AbilityEntry> e){
		e.getRegistry().register(new AbilityEntry(AbilityExitSuit.class, new ResourceLocation(IronMan.MODID, "exit_suit")));
		e.getRegistry().register(new AbilityEntry(AbilityFaceplate.class, new ResourceLocation(IronMan.MODID, "faceplate")));
		e.getRegistry().register(new AbilityEntry(AbilityToggleFlight.class, new ResourceLocation(IronMan.MODID, "toggle_flight")));
	}
}
