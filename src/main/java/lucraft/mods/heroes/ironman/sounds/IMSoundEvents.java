package lucraft.mods.heroes.ironman.sounds;

import lucraft.mods.heroes.ironman.IronMan;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@EventBusSubscriber(modid = IronMan.MODID)
public class IMSoundEvents {
	
	public static SoundEvent REPULSOR_ON;
	public static SoundEvent REPULSOR_OFF;
	public static SoundEvent REPULSOR_SHOT;
	
	@SubscribeEvent
	public static void onRegisterSounds(RegistryEvent.Register<SoundEvent> e) {
		e.getRegistry().register(REPULSOR_ON = new SoundEvent(new ResourceLocation(IronMan.MODID, "repulsor_on")).setRegistryName(new ResourceLocation(IronMan.MODID, "repulsor_on")));
		e.getRegistry().register(REPULSOR_OFF = new SoundEvent(new ResourceLocation(IronMan.MODID, "repulsor_off")).setRegistryName(new ResourceLocation(IronMan.MODID, "repulsor_off")));
		e.getRegistry().register(REPULSOR_SHOT = new SoundEvent(new ResourceLocation(IronMan.MODID, "repulsor_shot")).setRegistryName(new ResourceLocation(IronMan.MODID, "repulsor_shot")));
	}

}
