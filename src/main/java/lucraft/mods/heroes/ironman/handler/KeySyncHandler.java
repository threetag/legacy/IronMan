package lucraft.mods.heroes.ironman.handler;

import java.util.HashMap;
import java.util.Map;

import lucraft.mods.heroes.ironman.IronMan;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerChangedDimensionEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientDisconnectionFromServerEvent;

@EventBusSubscriber(modid = IronMan.MODID)
public class KeySyncHandler {
	
	private static final Map<EntityPlayer, Boolean> flyKeyState = new HashMap<EntityPlayer, Boolean>();
	private static final Map<EntityPlayer, Boolean> rightClickState = new HashMap<EntityPlayer, Boolean>();

	public static boolean isFlyKeyDown(EntityLivingBase user) {
		return !(user instanceof EntityPlayer) || flyKeyState.containsKey(user) && flyKeyState.get(user);
	}
	
	public static boolean isRightClicking(EntityLivingBase user) {
		return !(user instanceof EntityPlayer) || rightClickState.containsKey(user) && rightClickState.get(user);
	}

	public static void processKeyUpdate(EntityPlayer player, boolean keyFly, boolean rightClick) {
		flyKeyState.put(player, keyFly);
		rightClickState.put(player, rightClick);
	}

	public static void clearAll() {
		flyKeyState.clear();
		rightClickState.clear();
	}

	private static void removeFromAll(EntityPlayer player) {
		flyKeyState.remove(player);
		rightClickState.remove(player);
	}

	@SubscribeEvent
	public static void onPlayerLoggedOut(PlayerLoggedOutEvent evt) {
		removeFromAll(evt.player);
	}

	@SubscribeEvent
	public static void onDimChanged(PlayerChangedDimensionEvent evt) {
		removeFromAll(evt.player);
	}

	@SubscribeEvent
	public static void onClientDisconnectedFromServer(ClientDisconnectionFromServerEvent evt) {
		// SoundJetpack.clearPlayingFor();
	}
}