package lucraft.mods.heroes.ironman.handler;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.capabilities.CapabilityIronMan;
import lucraft.mods.heroes.ironman.capabilities.IIronManCapability;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPart;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.Random;

@EventBusSubscriber(modid = IronMan.MODID)
public class IMEventHandler {

//	@SubscribeEvent
//	public static void onLucCoreKey(AbilityKeyEvent.Server e) {
//		if (e.pressed && SuitSet.getSuitSet(e.player) == IMSuitSets.SENSOR_SUIT && !e.player.getCapability(CapabilityIronMan.IRON_MAN_CAP, null).getIronManSuitSetup().isEmpty()) {
//			IIronManCapability data = e.player.getCapability(CapabilityIronMan.IRON_MAN_CAP, null);
//
//			if (!data.getIronManSuitSetup().isEnabled())
//				return;
//
////			List<SuitAction> actions = data.getIronManSuitSetup().getSuitActions();
////			int keyId = e.type.ordinal();
////
////			if (keyId < actions.size()) {
////				actions.get(keyId).action(e.player, data, data.getIronManSuitSetup());
////			}
//		}
//	}

	@SubscribeEvent
	public static void onPlayerHurt(LivingHurtEvent e) {
		if (e.getSource() == DamageSource.FALL && e.getEntityLiving().getCapability(CapabilityIronMan.IRON_MAN_CAP, null) != null) {
			IronManSuitSetup setup = e.getEntityLiving().getCapability(CapabilityIronMan.IRON_MAN_CAP, null).getIronManSuitSetup();
			if (setup.isEmpty())
				return;
			e.setAmount(MathHelper.clamp(e.getAmount() - (setup.getDamageReduction() / 3F), 0, Integer.MAX_VALUE));
			if (e.getAmount() <= 0)
				e.setCanceled(true);
		}
	}

	@SubscribeEvent
	public static void onAttacked(LivingAttackEvent e) {
		if (e.getEntityLiving().isEntityInvulnerable(e.getSource()) || e.getEntityLiving().world.isRemote)
			return;
		IIronManCapability data = e.getEntityLiving().getCapability(CapabilityIronMan.IRON_MAN_CAP, null);

		if(data == null)
			return;

		IronManSuitSetup setup = data.getIronManSuitSetup();

		if (!setup.isEmpty()) {
			Random rand = new Random();

			if ((e.getSource() == DamageSource.ANVIL || e.getSource() == DamageSource.FALLING_BLOCK)) {
				if (!setup.getPart(IronManSuitPart.HELMET).isEmpty())
					setup.getPart(IronManSuitPart.HELMET).damageItem((int) (e.getAmount() * 4.0F + rand.nextFloat() * e.getAmount() * 2.0F), e.getEntityLiving());

				if (!setup.getPart(IronManSuitPart.FACEPLATE).isEmpty())
					setup.getPart(IronManSuitPart.FACEPLATE).damageItem((int) (e.getAmount() * 4.0F + rand.nextFloat() * e.getAmount() * 2.0F), e.getEntityLiving());
			}

			float damage = e.getAmount() / 7.0F;

			if (damage < 1.0F)
				damage = 1.0F;

			for (IronManSuitPart parts : IronManSuitPart.values()) {
				ItemStack itemstack = setup.getPart(parts);
				itemstack.damageItem((int) damage, e.getEntityLiving());
			}

			data.sync();
		}

		if (e.getAmount() <= 0)
			e.setCanceled(true);
	}

}
