package lucraft.mods.heroes.ironman.handler;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.network.IMPacketDispatcher;
import lucraft.mods.heroes.ironman.network.MessageSyncKeys;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;

@EventBusSubscriber(modid = IronMan.MODID, value = { Side.CLIENT })
public class IMClientEventHandler {

	public static Minecraft mc = Minecraft.getMinecraft();
	private static boolean lastFlyState = false;
	private static boolean lastRightClickState = false;
	
	@SubscribeEvent
	public static void onClientTick(TickEvent.ClientTickEvent evt) {
		if (evt.phase == TickEvent.Phase.START) {
			if (mc.player != null) {
				boolean flyState = mc.gameSettings.keyBindJump.isKeyDown();
				boolean rightClickState = mc.gameSettings.keyBindUseItem.isKeyDown();

				if (flyState != lastFlyState || lastRightClickState != rightClickState) {
					lastFlyState = flyState;
					lastRightClickState = rightClickState;
					IMPacketDispatcher.sendToServer(new MessageSyncKeys(flyState, rightClickState));
					KeySyncHandler.processKeyUpdate(mc.player, flyState, rightClickState);
				}
			}
		}
	}
	
}
