package lucraft.mods.heroes.ironman.items;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.lucraftcore.util.items.ItemBase;

public class ItemPalladiumCore extends ItemBase {

	public ItemPalladiumCore() {
		super("palladium_core");
		this.setCreativeTab(IronMan.TAB_IRON_MAN);
		this.setMaxStackSize(1);
		this.setMaxDamage(5*60*20);
	}

}
