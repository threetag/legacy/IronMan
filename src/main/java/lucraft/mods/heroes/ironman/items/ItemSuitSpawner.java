package lucraft.mods.heroes.ironman.items;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.IronManRegistries;
import lucraft.mods.heroes.ironman.entities.EntityIronMan;
import lucraft.mods.heroes.ironman.suits.IronManSuit;
import lucraft.mods.heroes.ironman.suits.IronManSuitSetup;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import lucraft.mods.lucraftcore.util.items.ExtendedTooltip;
import lucraft.mods.lucraftcore.util.items.ItemBase;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.*;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.energy.CapabilityEnergy;

import java.util.ArrayList;
import java.util.List;

public class ItemSuitSpawner extends ItemBase implements ExtendedTooltip.IExtendedItemToolTip {

	public ItemSuitSpawner() {
		super("suit_spawner");
		this.setCreativeTab(IronMan.TAB_IRON_MAN_SUITS);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		ItemStack itemstack = playerIn.getHeldItem(handIn);

		if (!playerIn.capabilities.isCreativeMode) {
			itemstack.shrink(1);
		}

		worldIn.playSound((EntityPlayer) null, playerIn.posX, playerIn.posY, playerIn.posZ, SoundEvents.ENTITY_SNOWBALL_THROW, SoundCategory.NEUTRAL, 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

		if (!worldIn.isRemote) {
			EntityIronMan suit = new EntityIronMan(worldIn, new IronManSuitSetup(itemstack.getTagCompound()));
			suit.setPosition(playerIn.posX, playerIn.posY, playerIn.posZ);
			Vec3d lookVec = playerIn.getLookVec();
			suit.motionX = lookVec.x / 2D;
			suit.motionZ = lookVec.z / 2D;
			suit.rotationPitch = playerIn.rotationPitch;
			suit.prevRotationPitch = playerIn.prevRotationPitch;
			suit.rotationYaw = playerIn.rotationYaw;
			suit.prevRotationYaw = playerIn.prevRotationYaw;
			suit.rotationYawHead = playerIn.rotationYawHead;
			suit.prevRotationYawHead = playerIn.prevRotationYawHead;
			suit.renderYawOffset = playerIn.renderYawOffset;
			suit.prevRenderYawOffset = playerIn.prevRenderYawOffset;
			suit.setOwnerId(playerIn.getPersistentID());
			worldIn.spawnEntity(suit);
		}

		playerIn.addStat(StatList.getObjectUseStats(this));
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		if (this.isInCreativeTab(tab)) {
			for (IronManSuit suit : IronManRegistries.IRON_MAN_SUIT_REGISTRY.getValues()) {
				ItemStack arcReactor = new ItemStack(IMItems.ARC_REACTOR);
				arcReactor.getCapability(CapabilityEnergy.ENERGY, null).receiveEnergy(arcReactor.getCapability(CapabilityEnergy.ENERGY, null).getMaxEnergyStored(), false);
				IronManSuitSetup setup = new IronManSuitSetup(ItemSuitPart.setIronManSuitPart(suit, new ItemStack(IMItems.SUIT_PART_HELMET)), ItemSuitPart.setIronManSuitPart(suit, new ItemStack(IMItems.SUIT_PART_FACEPLATE)), ItemSuitPart.setIronManSuitPart(suit, new ItemStack(IMItems.SUIT_PART_CHEST)), ItemSuitPart.setIronManSuitPart(suit, new ItemStack(IMItems.SUIT_PART_RIGHT_ARM)), ItemSuitPart.setIronManSuitPart(suit, new ItemStack(IMItems.SUIT_PART_LEFT_ARM)),
						ItemSuitPart.setIronManSuitPart(suit, new ItemStack(IMItems.SUIT_PART_RIGHT_LEG)), ItemSuitPart.setIronManSuitPart(suit, new ItemStack(IMItems.SUIT_PART_LEFT_LEG)), arcReactor);
				ItemStack stack = new ItemStack(this);
				stack.setTagCompound(setup.serializeNBT());
				items.add(stack);
			}
		}
	}

	@Override
	public boolean shouldShiftTooltipAppear(ItemStack stack, EntityPlayer player) {
		return true;
	}

	@Override
	public List<String> getShiftToolTip(ItemStack stack, EntityPlayer player) {
		List<String> list = new ArrayList<String>();

		if (!stack.hasTagCompound())
			return list;

		IronManSuitSetup setup = new IronManSuitSetup(stack.getTagCompound());
		list.add(StringHelper.translateToLocal("ironman.info.damage_reduction") + ": " + TextFormatting.GOLD + setup.getDamageReduction());
		list.add(StringHelper.translateToLocal("ironman.info.vertical_flight_speed") + ": " + TextFormatting.GOLD + setup.getVerticalFlyingSpeed());
		list.add(StringHelper.translateToLocal("ironman.info.horizontal_flight_speed") + ": " + TextFormatting.GOLD + setup.getHorizontalFlyingSpeed());
		return list;
	}

	@Override
	public boolean shouldCtrlTooltipAppear(ItemStack stack, EntityPlayer player) {
		return false;
	}

	@Override
	public List<String> getCtrlToolTip(ItemStack stack, EntityPlayer player) {
		return null;
	}
}
