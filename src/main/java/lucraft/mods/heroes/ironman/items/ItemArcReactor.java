package lucraft.mods.heroes.ironman.items;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.client.gui.IMGuiHandler;
import lucraft.mods.lucraftcore.util.items.ItemBaseEnergyStorage;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.IItemPropertyGetter;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

public class ItemArcReactor extends ItemBaseEnergyStorage {

	public ItemArcReactor() {
		super("arc_reactor", 5000000);
		this.setMaxStackSize(1);
		this.setCreativeTab(IronMan.TAB_IRON_MAN);
		
		this.addPropertyOverride(new ResourceLocation("energy"), new IItemPropertyGetter() {
			@SideOnly(Side.CLIENT)
			public float apply(ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entityIn) {
				double energy = 1D - stack.getItem().getDurabilityForDisplay(stack);
				return energy == 1D ? 1F : (energy == 0D ? 0F : (energy <= 0.25D ? 0.25F : (energy <= 0.5D ? 0.5F : (energy <= 0.75D ? 0.75F : 1F))));
			}
		});
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		if(handIn == EnumHand.MAIN_HAND && playerIn.getHeldItem(handIn).hasTagCompound()) {
			playerIn.openGui(IronMan.instance, IMGuiHandler.ARC_REACTOR_GUI_ID, worldIn, (int)playerIn.posX, (int)playerIn.posY, (int)playerIn.posZ);
			return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, playerIn.getHeldItem(handIn));
		}
		return new ActionResult<ItemStack>(EnumActionResult.PASS, playerIn.getHeldItem(handIn));
	}
	
	@Override
	public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		updateEnergy(stack);
	}
	
	public boolean updateEnergy(ItemStack stack) {
		if(!stack.hasTagCompound())
			stack.setTagCompound(new NBTTagCompound());
		
		InventoryArcReactor inventory = new InventoryArcReactor(stack);
		
		if(inventory.getStackInSlot(0) != null && !inventory.getStackInSlot(0).isEmpty() && inventory.getStackInSlot(0).getItem() == IMItems.PALLADIUM_CORE) {
			
			if(stack.getCapability(CapabilityEnergy.ENERGY, null).receiveEnergy(200, false) > 0) {
				if(inventory.getStackInSlot(0).attemptDamageItem(1, itemRand, null))
					inventory.getStackInSlot(0).shrink(1);
			}
				
			inventory.markDirty();
			return true;
		}
		
		return false;
	}

}
