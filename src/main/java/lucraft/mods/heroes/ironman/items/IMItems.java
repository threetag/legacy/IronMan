package lucraft.mods.heroes.ironman.items;

import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.client.render.item.ItemRendererSuitPart;
import lucraft.mods.heroes.ironman.client.render.item.ItemRendererSuitSpawner;
import lucraft.mods.heroes.ironman.items.ItemSuitPart.IronManSuitPart;
import lucraft.mods.heroes.ironman.suitsets.IMSuitSets;
import lucraft.mods.heroes.ironman.suitsets.IMSuitSets.IMSuitSet;
import lucraft.mods.lucraftcore.util.helper.ItemHelper;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.tileentity.TileEntityItemStackRenderer;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@EventBusSubscriber(modid = IronMan.MODID)
public class IMItems {

	public static Item SUIT_SPAWNER = new ItemSuitSpawner();
	public static Item SUIT_PART_HELMET = new ItemSuitPart(IronManSuitPart.HELMET);
	public static Item SUIT_PART_FACEPLATE = new ItemSuitPart(IronManSuitPart.FACEPLATE);
	public static Item SUIT_PART_CHEST = new ItemSuitPart(IronManSuitPart.CHEST);
	public static Item SUIT_PART_RIGHT_ARM = new ItemSuitPart(IronManSuitPart.RIGHT_ARM);
	public static Item SUIT_PART_LEFT_ARM = new ItemSuitPart(IronManSuitPart.LEFT_ARM);
	public static Item SUIT_PART_RIGHT_LEG = new ItemSuitPart(IronManSuitPart.RIGHT_LEG);
	public static Item SUIT_PART_LEFT_LEG = new ItemSuitPart(IronManSuitPart.LEFT_LEG);

	public static Item ARC_REACTOR = new ItemArcReactor();
	public static Item PALLADIUM_CORE = new ItemPalladiumCore();

	@SubscribeEvent
	public static void onRegisterItems(RegistryEvent.Register<Item> e) {
		for (IMSuitSet suit : IMSuitSets.SUIT_SETS) {
			suit.registerItems(e);
		}

		e.getRegistry().register(ARC_REACTOR);
		e.getRegistry().register(PALLADIUM_CORE);

		e.getRegistry().registerAll(SUIT_PART_HELMET, SUIT_PART_FACEPLATE, SUIT_PART_CHEST, SUIT_PART_RIGHT_ARM, SUIT_PART_LEFT_ARM, SUIT_PART_RIGHT_LEG, SUIT_PART_LEFT_LEG);
		e.getRegistry().register(SUIT_SPAWNER);
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public static void onRegisterModels(ModelRegistryEvent e) {
		for (IMSuitSet suit : IMSuitSets.SUIT_SETS) {
			suit.registerModels();
		}

		ItemHelper.registerItemModel(ARC_REACTOR, 0, new ModelResourceLocation(new ResourceLocation(IronMan.MODID, "arc_reactor"), "inventory"));
		ItemHelper.registerItemModel(PALLADIUM_CORE, 0, new ModelResourceLocation(new ResourceLocation(IronMan.MODID, "palladium_core"), "inventory"));

		TileEntityItemStackRenderer ARMOR_PART_RENDERER = new ItemRendererSuitPart();

		ItemHelper.registerItemModel(SUIT_PART_HELMET, IronMan.MODID, SUIT_PART_HELMET.getRegistryName().getPath());
		SUIT_PART_HELMET.setTileEntityItemStackRenderer(ARMOR_PART_RENDERER);

		ItemHelper.registerItemModel(SUIT_PART_FACEPLATE, IronMan.MODID, SUIT_PART_FACEPLATE.getRegistryName().getPath());
		SUIT_PART_FACEPLATE.setTileEntityItemStackRenderer(ARMOR_PART_RENDERER);

		ItemHelper.registerItemModel(SUIT_PART_CHEST, IronMan.MODID, SUIT_PART_CHEST.getRegistryName().getPath());
		SUIT_PART_CHEST.setTileEntityItemStackRenderer(ARMOR_PART_RENDERER);

		ItemHelper.registerItemModel(SUIT_PART_RIGHT_ARM, IronMan.MODID, SUIT_PART_RIGHT_ARM.getRegistryName().getPath());
		SUIT_PART_RIGHT_ARM.setTileEntityItemStackRenderer(ARMOR_PART_RENDERER);

		ItemHelper.registerItemModel(SUIT_PART_LEFT_ARM, IronMan.MODID, SUIT_PART_LEFT_ARM.getRegistryName().getPath());
		SUIT_PART_LEFT_ARM.setTileEntityItemStackRenderer(ARMOR_PART_RENDERER);

		ItemHelper.registerItemModel(SUIT_PART_RIGHT_LEG, IronMan.MODID, SUIT_PART_RIGHT_LEG.getRegistryName().getPath());
		SUIT_PART_RIGHT_LEG.setTileEntityItemStackRenderer(ARMOR_PART_RENDERER);

		ItemHelper.registerItemModel(SUIT_PART_LEFT_LEG, IronMan.MODID, SUIT_PART_LEFT_LEG.getRegistryName().getPath());
		SUIT_PART_LEFT_LEG.setTileEntityItemStackRenderer(ARMOR_PART_RENDERER);

		ItemHelper.registerItemModel(SUIT_SPAWNER, IronMan.MODID, "suit_spawner");
		SUIT_SPAWNER.setTileEntityItemStackRenderer(new ItemRendererSuitSpawner());
	}

}
