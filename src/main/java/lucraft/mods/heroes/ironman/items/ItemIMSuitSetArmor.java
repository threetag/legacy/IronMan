package lucraft.mods.heroes.ironman.items;

import lucraft.mods.lucraftcore.superpowers.suitsets.ItemSuitSetArmor;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import net.minecraft.inventory.EntityEquipmentSlot;

public class ItemIMSuitSetArmor extends ItemSuitSetArmor {

	public ItemIMSuitSetArmor(SuitSet suitSet, EntityEquipmentSlot armorSlot) {
		super(suitSet, armorSlot);
		
		this.setTranslationKey(suitSet.getUnlocalizedName() + getArmorSlotName(armorSlot));
		this.setRegistryName(suitSet.getRegistryName() + "_" + getArmorSlotName(armorSlot).toLowerCase());
	}

}
