package lucraft.mods.heroes.ironman.items;

import com.google.gson.JsonObject;
import lucraft.mods.heroes.ironman.IronMan;
import lucraft.mods.heroes.ironman.IronManRegistries;
import lucraft.mods.heroes.ironman.client.models.ModelIronManSuit;
import lucraft.mods.heroes.ironman.suits.IronManSuit;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import lucraft.mods.lucraftcore.util.items.ExtendedTooltip;
import lucraft.mods.lucraftcore.util.items.ItemBase;
import lucraft.mods.lucraftcore.util.render.LCModelState;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.block.model.ItemTransformVec3f;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.model.IModelState;
import net.minecraftforge.common.model.TRSRTransformation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemSuitPart extends ItemBase implements ExtendedTooltip.IExtendedItemToolTip {

	public final IronManSuitPart part;

	public ItemSuitPart(IronManSuitPart part) {
		super("suit_part_" + part.toString().toLowerCase());
		this.setCreativeTab(IronMan.TAB_IRON_MAN_SUITS);
		this.setMaxStackSize(1);
		this.part = part;
	}

	@Override
	public String getItemStackDisplayName(ItemStack stack) {
		return super.getItemStackDisplayName(stack).replace("%SUIT", (getIronManSuit(stack) != null ? getIronManSuit(stack).getDisplayName().getFormattedText() : ""));
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		if (this.isInCreativeTab(tab)) {
			for (IronManSuit suit : IronManRegistries.IRON_MAN_SUIT_REGISTRY.getValues()) {
				if (suit.getSuitPartInfo(part).isEnabled()) {
					ItemStack stack = new ItemStack(this);
					setIronManSuitPart(suit, stack);
					items.add(stack);
				}
			}
		}
	}

	@Override
	public int getDamage(ItemStack stack) {
		return stack.hasTagCompound() ? stack.getTagCompound().getInteger("Damage") : 0;
	}

	@Override
	public int getMaxDamage(ItemStack stack) {
		IronManSuit suit = getIronManSuit(stack);

		if (suit == null)
			return 0;

		return suit.getSuitPartInfo(part).getDurability();
	}

	@Override
	public void setDamage(ItemStack stack, int damage) {
		NBTTagCompound nbt = stack.hasTagCompound() ? stack.getTagCompound() : new NBTTagCompound();
		nbt.setInteger("Damage", MathHelper.clamp(damage, 0, getMaxDamage(stack)));
		stack.setTagCompound(nbt);
	}

	@Override
	public boolean isDamaged(ItemStack stack) {
		return getDamage(stack) > 0;
	}

	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		return (double) getDamage(stack) / (double) getMaxDamage(stack);
	}

	@Override
	public boolean showDurabilityBar(ItemStack stack) {
		return isDamaged(stack);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

	public static ItemStack setIronManSuitPart(IronManSuit suit, ItemStack stack) {
		if (!stack.isEmpty() && suit != null && stack.getItem() instanceof ItemSuitPart) {
			NBTTagCompound nbt = stack.hasTagCompound() ? stack.getTagCompound() : new NBTTagCompound();
			nbt.setString("Suit", suit.getRegistryName().toString());
			nbt.setInteger("Damage", 0);
			stack.setTagCompound(nbt);
			return stack;
		}

		return ItemStack.EMPTY;
	}

	public static IronManSuit getIronManSuit(ItemStack stack) {
		if (!stack.isEmpty() && stack.getItem() instanceof ItemSuitPart && stack.hasTagCompound()) {
			return IronManRegistries.IRON_MAN_SUIT_REGISTRY.getValue(new ResourceLocation(stack.getTagCompound().getString("Suit")));
		}

		return null;
	}

	@Override
	public boolean shouldShiftTooltipAppear(ItemStack stack, EntityPlayer player) {
		return true;
	}

	@Override
	public List<String> getShiftToolTip(ItemStack stack, EntityPlayer player) {
		List<String> list = new ArrayList<String>();
		IronManSuit suit = getIronManSuit(stack);
		IronManSuitPartInfo info = suit.getSuitPartInfo(part);
		list.add(StringHelper.translateToLocal("ironman.info.durability") + ": " + TextFormatting.GOLD + (getMaxDamage(stack) - getDamage(stack)) + TextFormatting.DARK_GRAY + "/" + TextFormatting.GOLD + getMaxDamage(stack));
		list.add(StringHelper.translateToLocal("ironman.info.damage_reduction") + ": " + TextFormatting.GOLD + info.getDamageReduction());
		list.add(StringHelper.translateToLocal("ironman.info.vertical_flight_speed") + ": " + TextFormatting.GOLD + info.getVerticalFlyingSpeed());
		list.add(StringHelper.translateToLocal("ironman.info.horizontal_flight_speed") + ": " + TextFormatting.GOLD + info.getHorizontalFlyingSpeed());
		return list;
	}

	@Override
	public boolean shouldCtrlTooltipAppear(ItemStack stack, EntityPlayer player) {
		return false;
	}

	@Override
	public List<String> getCtrlToolTip(ItemStack stack, EntityPlayer player) {
		return null;
	}

	public static enum IronManSuitPart {

		HELMET, FACEPLATE, CHEST, RIGHT_ARM, LEFT_ARM, RIGHT_LEG, LEFT_LEG;

		public ResourceLocation getResourceLocation() {
			return new ResourceLocation(IronMan.MODID, "suit_part_" + toString().toLowerCase());
		}

		public ModelResourceLocation getModelResourceLocation() {
			return new ModelResourceLocation(getResourceLocation(), "inventory");
		}

		@SideOnly(Side.CLIENT)
		public IModelState getTransforms() {
			Map map = new HashMap<ItemCameraTransforms.TransformType, TRSRTransformation>();

			if (this == IronManSuitPart.HELMET) {
				map.put(TransformType.GUI, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(10F, -30F, 0), new Vector3f(0.5F, 0.25F, 0), new Vector3f(0.9F, 0.9F, 0.9F))));
				map.put(TransformType.GROUND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 0F, 0F), new Vector3f(0.5F, 0.2F, 0.5F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.8F, 0.4F, 0F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.2F, 0.4F, -0.8F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(90F, 180F, 0F), new Vector3f(0.5F, 0.4F, -0F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(90F, 180F, 0F), new Vector3f(1.5F, 0.4F, -0F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIXED, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.5F, 0.3F, 0.5F), new Vector3f(1F, 1F, 1F))));
			}

			else if (this == IronManSuitPart.FACEPLATE) {
				map.put(TransformType.GUI, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(10F, -30F, 0), new Vector3f(0.6F, 0.25F, 0F), new Vector3f(0.9F, 0.9F, 0.9F))));
				map.put(TransformType.GROUND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(-90F, 0F, 0F), new Vector3f(0.5F, 0.1F, 0.75F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.8F, 0.4F, 0F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.2F, 0.4F, -0.8F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(50F, 180F, 0F), new Vector3f(0.5F, 0.1F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(50F, 180F, 0F), new Vector3f(1.5F, 0.1F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIXED, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.5F, 0.3F, 0.8F), new Vector3f(1F, 1F, 1F))));
			}

			else if (this == IronManSuitPart.CHEST) {
				map.put(TransformType.GUI, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(10F, -30F, 0), new Vector3f(0.5F, 0.8F, 0), new Vector3f(0.9F, 0.9F, 0.9F))));
				map.put(TransformType.GROUND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(-90F, 0F, 0F), new Vector3f(0.5F, 0.3F, 0.1F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.8F, 0.9F, 0F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.2F, 0.9F, -0.8F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.5F, 0.7F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(1.5F, 0.7F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIXED, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.5F, 0.85F, 0.5F), new Vector3f(1F, 1F, 1F))));
			}

			else if (this == IronManSuitPart.RIGHT_ARM) {
				map.put(TransformType.GUI, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(10F, -30F, 0), new Vector3f(0.6F, 0.8F, 0), new Vector3f(0.9F, 0.9F, 0.9F))));
				map.put(TransformType.GROUND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(-90F, 0F, 0F), new Vector3f(0.9F, 0.3F, 0.1F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(1.5F, 0.9F, 0F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.2F, 0.9F, -1.3F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.15F, 0.7F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(1.9F, 0.7F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIXED, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.5F, 0.85F, 0.5F), new Vector3f(1F, 1F, 1F))));
			}

			else if (this == IronManSuitPart.LEFT_ARM) {
				map.put(TransformType.GUI, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(10F, -30F, 0), new Vector3f(0.4F, 0.8F, 0), new Vector3f(0.9F, 0.9F, 0.9F))));
				map.put(TransformType.GROUND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(-90F, 0F, 0F), new Vector3f(0.1F, 0.3F, 0.1F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(1.1F, 0.9F, -0.6F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.9F, 0.9F, -0.8F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.15F, 0.7F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(1.9F, 0.7F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIXED, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.5F, 0.85F, 0.5F), new Vector3f(1F, 1F, 1F))));
			}

			else if (this == IronManSuitPart.RIGHT_LEG) {
				map.put(TransformType.GUI, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(10F, -30F, 0), new Vector3f(0.4F, 1.5F, 0), new Vector3f(0.9F, 0.9F, 0.9F))));
				map.put(TransformType.GROUND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(-90F, 0F, 0F), new Vector3f(0.6F, 0.3F, -0.6F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(1F, 1.7F, 0F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.2F, 1.7F, -1F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.4F, 1.5F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(1.65F, 1.5F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIXED, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.5F, 1.6F, 0.5F), new Vector3f(1F, 1F, 1F))));
			}

			else if (this == IronManSuitPart.LEFT_LEG) {
				map.put(TransformType.GUI, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(10F, -30F, 0), new Vector3f(0.6F, 1.45F, 0), new Vector3f(0.9F, 0.9F, 0.9F))));
				map.put(TransformType.GROUND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(-90F, 0F, 0F), new Vector3f(0.4F, 0.3F, -0.6F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.9F, 1.7F, -0.24F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIRST_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.3F, 1.7F, -0.8F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.65F, 1.5F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.THIRD_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(1.41F, 1.5F, 0.4F), new Vector3f(1F, 1F, 1F))));
				map.put(TransformType.FIXED, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.5F, 1.6F, 0.5F), new Vector3f(1F, 1F, 1F))));
			}

			return new LCModelState(map);
		}

		@SideOnly(Side.CLIENT)
		public void setModelVisibilities(ModelIronManSuit model) {
			model.setVisible(false);

			if (this == IronManSuitPart.HELMET) {
				model.bipedHead = new ModelRenderer(model, 0, 0);
				model.bipedHead.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0);
				model.bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
				model.bipedHead.addChild(model.bipedHeadplate);
				model.bipedHead.addChild(model.bipedHeadplateWear);
				model.bipedHead.showModel = true;
				model.bipedHeadwear.showModel = true;
				model.bipedHeadplate.showModel = false;
				model.bipedHeadplateWear.showModel = false;

			} else if (this == IronManSuitPart.FACEPLATE) {
				model.bipedHead = new ModelRenderer(model, -200, -200);
				model.bipedHead.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0);
				model.bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
				model.bipedHead.addChild(model.bipedHeadplate);
				model.bipedHead.addChild(model.bipedHeadplateWear);
				model.bipedHead.showModel = true;
				model.bipedHeadplate.showModel = true;
				model.bipedHeadplateWear.showModel = true;

			} else if (this == IronManSuitPart.CHEST) {
				model.bipedBody.showModel = true;
				model.bipedBodyWear.showModel = true;

			} else if (this == IronManSuitPart.RIGHT_ARM) {
				model.bipedRightArm.showModel = true;
				model.bipedRightArmwear.showModel = true;

			} else if (this == IronManSuitPart.LEFT_ARM) {
				model.bipedLeftArm.showModel = true;
				model.bipedLeftArmwear.showModel = true;

			} else if (this == IronManSuitPart.RIGHT_LEG) {
				model.bipedRightLeg.showModel = true;
				model.bipedRightLegwear.showModel = true;

			} else if (this == IronManSuitPart.LEFT_LEG) {
				model.bipedLeftLeg.showModel = true;
				model.bipedLeftLegwear.showModel = true;

			}
		}

		public static IronManSuitPart getPartFromName(String name) {
			for (IronManSuitPart part : IronManSuitPart.values()) {
				if (part.toString().equalsIgnoreCase(name)) {
					return part;
				}
			}

			return null;
		}

	}

	public static class IronManSuitPartInfo {

		protected boolean enabled;
		protected boolean hud;
		protected int durability;
		protected float damageReduction;
		protected float verticalFlyingSpeed;
		protected float horizontalFlyingSpeed;

		public IronManSuitPartInfo(boolean enabled, boolean hud, int durability, float damageReduction, float verticalFlyingSpeed, float horizontalFlyingSpeed) {
			this.enabled = enabled;
			this.hud = hud;
			this.durability = durability;
			this.damageReduction = damageReduction;
			this.verticalFlyingSpeed = verticalFlyingSpeed;
			this.horizontalFlyingSpeed = horizontalFlyingSpeed;
		}

		public boolean isEnabled() {
			return enabled;
		}
		
		public boolean showHud() {
			return hud;
		}

		public int getDurability() {
			return durability;
		}

		public float getDamageReduction() {
			return damageReduction;
		}

		public float getVerticalFlyingSpeed() {
			return verticalFlyingSpeed;
		}

		public float getHorizontalFlyingSpeed() {
			return horizontalFlyingSpeed;
		}

		public static IronManSuitPartInfo deserialize(JsonObject json) {
			boolean b = JsonUtils.getBoolean(json, "enabled");
			boolean hud = JsonUtils.hasField(json, "hud") ? JsonUtils.getBoolean(json, "hud") : true;
			int durability = JsonUtils.getInt(json, "durability");
			float damageReduction = JsonUtils.hasField(json, "damage_reduction") ? JsonUtils.getFloat(json, "damage_reduction") : 0;
			float verticalFlyingSpeed = JsonUtils.hasField(json, "vertical_flying_speed") ? JsonUtils.getFloat(json, "vertical_flying_speed") : 0;
			float horizontalFlyingSpeed = JsonUtils.hasField(json, "horizontal_flying_speed") ? JsonUtils.getFloat(json, "horizontal_flying_speed") : 0;
			return new IronManSuitPartInfo(b, hud, durability, damageReduction, verticalFlyingSpeed, horizontalFlyingSpeed);
		}

	}

}
